<?php

class login
{
	var $username;
	var $password;
	var $err;
	var $table_name;
	var $session_data;
	var $md5 = true;
	var $username_column = "username";
	var $password_column = "password";
	
	function login($username,$password)
	{
		$this->username = $username;
		$this->password = $password;
		$this->builder = new queryBuilder();
	}
	
	function set_table($tablename)
	{
		$this->table_name = $tablename;
	}
	
	/*
	 * Tells the login class where to find the username and password in database table
	 */
	function set_columns($username_col,$password_col)
	{
		$this->username_column = $username_col;
		$this->password_column = $password_col;
	}
	
	function md5_on()
	{
		$this->md5 = true;
	}
	
	function md5_off()
	{
		$this->md5 = false;
	}
	
	function authenticate()
	{
		$db = new mySQLConnection();
		$db->select();
		// if md5 is turned on
		if($this->md5)
			$this->password = md5($this->password);
		
		$this->builder->set_type("SELECT");
		$this->builder->set_table_name($this->table_name);
		$this->builder->set_where("WHERE $this->username_column = '$this->username' and $this->password_column = '$this->password'");
		
		$query = $this->builder->build_query();
		if($db->execute_query($query))
			$data = $db->fetch($db->result);
		else
		{
			die($db->error);
		}
		if($db->rows_affected() == 1)
		{
		    $this->session_data = $data[0];
		    return true;
		}
		else
		{
		    $this->err = "Invalid username or password. Please try again";
		    return false;
		}
	}
	
	function get_error()
	{
		return $this->err;
	}
	
}

?>