<?php
class formPopulator
{
	var $json;
	
	private function formPopulator()
	{
		if(isset($_POST))
			$form_data = $_POST;
		elseif(isset($_GET))
			$form_data = $_GET;
			
		$i = 0;
		foreach($form_data as $id => $value)
		{
			$json[$i]['id'] = $id;
			$json[$i]['value'] = $value;
			$i++;
		}
		
		$this->json = json_encode($json);
	}
	
	static function populate()
	{
		$fp = new formPopulator();
		
		$js = "
		<script type=\"text/javascript\">
		var a = $fp->json;
		var field;
		for(i = 0; i < a.length; i++)
		{
			if(field = document.getElementById(a[i].id))
			{
				switch(field.type)
				{
					case \"text\":
						field.value = a[i].value;
						break;
					case \"textarea\":
						field.value = a[i].value;
						break;
					case \"radio\":
						alert(a[i].value);
						break;
					case \"checkbox\":
						field.checked = true;
						break;
					case \"select-one\":
						for(var j = 0; j < field.options.length; j++)
						{
							if(field.options[j].text == a[i].value)
					  			field.options[j].selected = true;
						}
						break;
						
				}
			}
			else if(field = document.getElementById(a[i].value))
			{
				switch(field.type)
				{
					case \"radio\":
						field.checked = true;
						break;				
				}
			}
		}
		</script>";
				
				return $js;
	}
}