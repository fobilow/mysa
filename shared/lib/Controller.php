<?php
class Controller
{
    var $link;
    
    var $controller = 'index';
    
    var $action = 'index';
    
    function Controller()
    {
        if(isset($_GET['action']))
            $this->action = $_GET['action'];
    }
    
    function set_action($action)
    {
        $action_link = $this->link."&action=".$action;
        return $action_link;
    }
    
	function run()
    {
        $controller = $this->controller."Controller"; 
        
        if(!is_file(CONTROLLERPATH."/$controller.php"))
            exit("<b>$controller</b> does not exist - ensure that you have created file <b>$controller.php</b> in the controllers directory");
        
        $this->{$this->action}();  
    }
    
    //incase an unknown method is called, this will redirect to the index
    function __call($m,$a)
    {
        $class = get_class($this);
		echo "unknown method <b>$m</b> was called on <b>{$class}</b> object - check that the method has been defined in the {$class} class<br>";
    }
    
    function xray($var)
    {
    	echo "<pre>";
		print_r($var);
		echo "</pre>";
    }
	
	function get_keywords()
	{
		if(isset($_POST['search_string']))
		{
			$search_string = $_GET['search_string'];
			
			$keywords = explode(" ",$search_string);
			if(!in_array($search_string,$keywords))
				array_push($keywords,$search_string);
				
			return $keywords;
		}
		else
		{
			header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
		}
	}
    
}
        
  
?>
