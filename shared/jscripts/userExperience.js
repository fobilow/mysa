/////////////////////////////////////////////////////////////////////////////////////////
//				This scripts conatins function that make user				  	////////
////			expperience with web aplication as wonderful 					///////
/////			as possible. 													//////
//////																			/////							
///////			author: okechukwu ugwu											////
////////		email: fo_bilow@yahoo.com										///
//////////		last modified: 4/03/2008										//
/////////////////////////////////////////////////////////////////////////////////

function selectAll(name)
{
	checkList  = document.getElementsByName(name);
	for(i = 0; i < checkList.length; i++)
	{
	checkList[i].checked = true;
	/*hilite(checkList[i]);*/
	}
}

function unselectAll(name)
{
	checkList  = document.getElementsByName(name);
	for(i = 0; i < checkList.length; i++)
	{
	checkList[i].checked = false;
	/*hilite(checkList[i]);*/
	}
}



function checkAll(name,obj)
{
	checkList  = document.getElementsByName(name);
		if(obj.checked)
		{
			for(i = 0; i < checkList.length; i++)
			{
			checkList[i].checked = true;
			/*hilite(checkList[i]);*/
			}
		}
		else
		{
			for(i = 0; i < checkList.length; i++)
			{
			checkList[i].checked = false;
			/*hilite(checkList[i]);*/
			}
		}
	
	toggle_edit_delete(name,'edit','delete');
}

/*this function is very specific to thirdeye, no plans to resuse it */
function checkAllAndEnableTeacher(obj,check)
{
		if (check === undefined ) 
			checkList  = document.getElementsByName("subject_id[]");
		else
			checkList  = document.getElementsByName(check);
		
		teacherList = document.getElementsByName("teacher_id[]"); 
		if(obj.checked)
		{
			for(i = 0; i < checkList.length; i++)
			{
				checkList[i].checked = true;
				teacherList[i].disabled = false;
			}
		}
		else
		{
			for(i = 0; i < checkList.length; i++)
			{
				checkList[i].checked = false;
				teacherList[i].disabled = true;
			}
		}
	
}

function enableElementById(id,obj)
{
	element = document.getElementById(id);
		
	if (obj === undefined ) 
	{
		 if(element.disabled)
				element.disabled = false;
		else
				element.disabled = true;
	}
	else
	{
		if(obj.checked)
			element.disabled = false;
		else 
			element.disabled = true;
	}
		
		
}


function enableElementsByName(name)
{
		elements = document.getElementsByName(name);
		for(i = 0; i < elements.length; i++)
		{
			if(elements[i].disabled)
				elements[i].disabled = false;
			else
				elements[i].disabled = true;
		}
}

function hilite(obj)
{
		row = document.getElementById(obj.value);
		if(obj.checked)
		row.style.backgroundColor = '#99CCFF';
		else
		row.style.backgroundColor = '';
}

function hiliteRow($id)
{
	row = document.getElementById($id);
	row.style.backgroundColor = '#E2E2E2';
	//row.style.fontWeight = '500';
}

function unhiliteRow($id,$color)
{
	if($color == null)
		$color = "#FFFFFF";
	row = document.getElementById($id);
	row.style.backgroundColor = $color;
	//row.style.fontWeight = 'normal';
}
	

function enableForm(formid)
{
	form = document.getElementById(formid);
	for(i = 0; i <= form.length; i++)
	{
		form[i].disabled = true;
	}
}

function submitForm()
{
	document.submit();
	//alert('BUZZ!');
}

function enable(id,name)
{
	element = document.getElementById(id); // the element 
	checkList  = document.getElementsByName(name);
	noneChecked = true;
	for(i = 0; i < checkList.length; i++)
	{
		if(checkList[i].checked)
		{
			noneChecked = false; // there is at least one checked check box
			break;
		}
	}
	
	if(noneChecked)
		element.disabled = true; // disable element
	else
		element.disabled = false; // enable element
}

function toggle_edit_delete(name,edit_btn, delete_btn)
{
	//how many check boxes are checked?
	checked = 0; //intialize to 0
	checkList  = document.getElementsByName(name);
	for(i = 0; i < checkList.length; i++)
	{
		if(checkList[i].checked)
		{
			checked ++;
		}
	}
	
	if(checked == 0)
	{
		//disable both edit and delete button
		document.getElementById(edit_btn).disabled = true;
		document.getElementById(delete_btn).disabled = true;
	}
	
	//if checked greater than 1
	if(checked > 1)
	{
		//enable only delete button
		document.getElementById(edit_btn).disabled = true;
		document.getElementById(delete_btn).disabled = false;
	}
	else
	{
		if(checked == 1)
		{
			//enable both edit and delete button
			document.getElementById(edit_btn).disabled = false;
			document.getElementById(delete_btn).disabled = false;
		}
	}
}

		

function openWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function confirmAction($obj,$action_url,$msg)
{
		if(confirm($msg))
		$obj.href = $action_url;
}

function confirmButtonAction($action_url,$msg)
{
		if(confirm($msg))
		location.href = $action_url;
}

function showRegion(region)
{
	region_div = document.getElementById(region);
	region_div.style.visibility = 'visible';
}

function showHide(show,hide,obj)
{
	if(obj.checked)
	{
		document.getElementById(show).style.visibility = 'visible';
		document.getElementById(show).style.display = 'inherit';
		document.getElementById(hide).style.visibility = 'hidden';
	}
	else
	{
		document.getElementById(show).style.visibility = 'hidden';
		document.getElementById(show).style.display = 'none';
		document.getElementById(hide).style.visibility = 'visible';
	}
}
	