<?php
/**
 * This class represents a school and its information
 * @author FO
 * @version 1.0
 * @package thirdeye
 */
class school extends Model
{
	var $table_name = "school";
	
	
	var $primary_key = "school_name";
	
	function insert($data)
	{
		$data = parent::filter_input($data);
		
		$school_name = $data['school_name'];
		$school_admin = $data['school_admin'];
		$school_email = $data['school_email'];
		$school_phoneno = $data['school_phoneno'];
		$school_address = $data['school_address'];
		
		$this->mysql->execute_query("DELETE FROM $table_name");
		$sql = "INSERT INTO `school` (`school_name`,`school_admin`,`school_email`,`school_phoneno`,`school_address`)
			VALUES ('$school_name','$school_admin','$school_email','$school_phoneno','$school_address')
			ON DUPLICATE KEY UPDATE school_name = '$school_name', school_admin = '$school_admin',
			school_email = '$school_email', school_phoneno = '$school_phoneno', school_address = '$school_address'
			";
          
		//echo $sql; exit;
		
		if (!$this->mysql->execute_query($sql))
            		echo get_class($this)."-".$this->mysql->error;
	}
	
	
	function update_logo($data)
	{
		$data = parent::filter_input($data);
		
		$school_name = $data['school_name'];
	
        $school_logo = $data['school_logo'];
		
        $sql = "UPDATE `school` SET `school_logo` = '$school_logo'
    			WHERE school_name = '$school_name'";
				
		if (!$this->mysql->execute_query($sql))
            		echo get_class($this)."-".$this->mysql->error;
    }
	
	function get_account()
	{
		return false;
	}
	
}

?>
