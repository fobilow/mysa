<?php
/**
 * @author FO
 * @version 1.0
 * @package thirdeye
 */
class session extends Model
{
	var $table_name = "session";
	
	function insert($data)
	{
		$data = parent::filter_input($data);
		$mf = $data['month_from'];
		$mt = $data['month_to'];
		$term = $data['term'];
		
		
		$sql = "INSERT INTO `session` (`month_from`,`month_to`,`term`) 
    					VALUES ('$mf','$mt','$term')
    					ON DUPLICATE KEY UPDATE month_from = '$mf', month_to = '$mt', term = '$term'
    					";
		//echo $sql;
		
		if (!$this->mysql->execute_query($sql))
            		echo get_class($this)."-".$this->mysql->error;
	}
	
	/**
	 * This function returns the current session and term
	 * @return unknown_type
	 */
	function get_current_session_term()
	{
		$current_month = date('n');		
		$sql = "SELECT term FROM `session` WHERE $current_month between month_from and month_to";
		
		if (!$this->mysql->execute_query($sql))
            		echo get_class($this)."-".$this->mysql->error;
		else
		{
			$r = $this->mysql->fetch($this->mysql->result);
			$st['term'] = $r[0]['term'];
			$current_year = date('Y');
			$prev_year = date('Y') - 1;
			$st['session'] = $prev_year.'/'.$current_year;
			return $st;		
		}
		
			

	}
}
