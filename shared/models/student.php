<?php
/**
 * @author FO
 * @version 1.0
 * @package 
 */
class student extends Model
{
	var $table_name = "students";
	
	var $primary_key = "id";
	
	var $per_page = 15;
	
	function select_all(array $select_column = null, $start = null,$where = null)
	{
		$qb = new queryBuilder();
		$qb->set_table_name($this->get_table_name());
		$qb->set_type("SELECT");
		if (!is_null($select_column)) {
		    $qb->set_field($select_column);
		}
		if (!is_null($start)) {
		    $rpp = $this->get_record_per_page(); 
		    $qb->set_limit("LIMIT $start,$rpp");
		}
		if (!is_null($where)) {
		    $rpp = $this->get_record_per_page();
		    $qb->set_where($where);
		}

		$qb->set_order("ORDER BY firstname");

		$sql = $qb->build_query();
		//echo $sql;
		if (!$this->mysql->execute_query($sql))
		    echo get_class($this)."-".$this->mysql->error;
		else
		    return $this->mysql->fetch($this->mysql->result);
	}
	    
	function insert($data)
	{
		$student_id = $data['student_id'];
		if(!$this->student_id_exists($student_id))
		{
			return parent::insert($data);
		}
		else
		{
			die("Student id already exist. Cannot have duplicate id's");
		}
	}
	
	/*function update($id,$data)
	{
		$student_id = $data['student_id'];
		if(!$this->student_id_exists($student_id))
		{
			parent::update($id,$data);
		}
		else
		{
			die("Student id already exist. Canot have duplicate id's");
		}
	}*/
	
	
	/**
	 * Generates a session id for a student, used as the primary key for registered students, if you pass only the student id
	 * to the function, it will try to generate a session id based on the current session and term already configured
	 * @param int $student_id
	 * @param string $session
	 * @param int $term
	 * @return string session id
	 */
	private function generate_session_id($student_id,$session = null,$term = null)
	{	
		if(is_null($session) and is_null($term))
		{
			$sess = new session();
			$sess_term = $sess->get_current_session_term();
			$session = $sess_term['session'];
			$term = $sess_term['term'];
		}
		
		
		/* converts e.g 2008/2009 to 0809 */
		$sid = substr($session,2,2)."".substr($session,7,2);
		
		/* putting every parameter into an array, for the join function */
		$id = array($student_id,$sid,$term);
		
		return join("-",$id);
	}
	
	function unregister($student_id)
	{
		//1. compute the session id
		//2. delete registered subjects
		//3. delete gradebook
		//4. delete student class entry
		//4. just to make sure i wipe out any attendance record
		
		$session_id = $this->generate_session_id($student_id);
		
		$sql1 = "DELETE FROM `student_subjects` 
				 WHERE session_id = '$session_id'";
		
		$sql2 = "DELETE FROM `grades` 
				 WHERE session_id = '$session_id'";
		
		$sql3 = "DELETE FROM `student_class` 
				 WHERE session_id = '$session_id'";
		
		$sql4 = "DELETE FROM `attendance` 
				 WHERE session_id = '$session_id'";
		
		if (!$this->mysql->execute_query($sql1) || 
			!$this->mysql->execute_query($sql2) ||
			!$this->mysql->execute_query($sql3) ||
			!$this->mysql->execute_query($sql4))
		{
            echo get_class($this)."-".$this->mysql->error;
            return;
		}
		
	}
	
	/**
	 * This function wraps the register_class(),register_subjects() and create_gradebook() function.
	 * @param unknown_type $post
	 * @return unknown_type
	 */
	function register($post)
	{
		$subjects = $post['subjects'];
	
		$session_id = $this->register_class($post);
		
		for($i = 0; $i < count($subjects); $i++)
		{
			//insert $session_id and $subjects[$i] into the studentSubjects table.
			$subject_id = $subjects[$i];
			$this->register_subjects($session_id, $subject_id);
			$this->create_gradebook($session_id,$subject_id);
		}
		
	}
	
	
	/**
	 * Registers a student to a class
	 * @param $post
	 * @return String - returns the session id 
	 */
	function register_class($post)
	{
		$id = $post['id'];
		$session = $post['session'];
		$term = $post['term'];
		$class_id = $post['class_id'];
		$mode = $post['mode'];
		$session_id = $this->generate_session_id($id,$session,$term);
		
		$sql = "INSERT INTO `student_class` (`session_id`, `student_id`,`class_id`,`session`,`term`,`mode`) 
				VALUES ('$session_id','$id','$class_id','$session','$term','$mode')";
		
		if (!$this->mysql->execute_query($sql))
            echo get_class($this)."-".$this->mysql->error;
        else
            return $session_id;
	}
	
	/**
	 * registers subjects to a student
	 * @param $session_id
	 * @param $subject_id
	 */
	function register_subjects($session_id,$subject_id)
	{
		$sql = "INSERT INTO `student_subjects` (`session_id`, `subject_id`) 
				VALUES ('$session_id','$subject_id')";
		if (!$this->mysql->execute_query($sql))
            echo get_class($this)."-".$this->mysql->error;
	}
	
	function create_gradebook($session_id,$subject_id)
	{
		$sql = "INSERT INTO `grades` (`session_id`, `subject_id`) 
				VALUES ('$session_id','$subject_id')";
		if (!$this->mysql->execute_query($sql))
            echo get_class($this)."-".$this->mysql->error;
	}
	
	function pick_subject($student_id,$subject_id)
	{
		$session_id = $this->generate_session_id($student_id);
		
		$sql = "INSERT INTO `student_subjects` (`session_id`, `subject_id`) 
				VALUES ('$session_id','$subject_id')";
		if (!$this->mysql->execute_query($sql))
		{
            echo get_class($this)."-".$this->mysql->error;
            return;
		}
            
        //add subject to gradebook
        $this->create_gradebook($session_id,$subject_id);
	}
	
	function drop_subject($student_id,$subject_id)
	{
		$session_id = $this->generate_session_id($student_id);
		
		$sql1 = "DELETE FROM `student_subjects` 
				 WHERE session_id = '$session_id' and subject_id = '$subject_id'";
		
		$sql2 = "DELETE FROM `grades` 
				 WHERE session_id = '$session_id' and subject_id = '$subject_id'";
		
		//if any of the query fail, return error message
		if (!$this->mysql->execute_query($sql1) || !$this->mysql->execute_query($sql2))
		{
            echo get_class($this)."-".$this->mysql->error;
            return;
		}
	}
	
	/** 
	 * This function gets student's grades for all subjects
	 * @param int $student_id	
	 * @param string $session
	 * @param int $term
	 * @return Array an array containing students scores 
	*/
	function get_grades($student_id, $session = null, $term = null)
	{
		if(is_null($session) and is_null($term))
		{
			$sess = new session();
			$sess_term = $sess->get_current_session_term();
			$session = $sess_term['session'];
			$term = $sess_term['term'];
		}
		
		$asmt = new assesment();
		$asmt_string = $asmt->assesment_string();

		if($asmt_string != '')
		{
			$sql = "SELECT firstname, lastname, gender, students.id, students.student_id, student_class.session_id, student_class.class_position, class_id, session, term, student_class.comment as pcomment, subjects.name, grades.*, ($asmt_string) as total
				FROM students, subjects, student_class  
				LEFT OUTER JOIN grades 
				ON  grades.session_id = student_class.session_id  
				WHERE grades.subject_id = subjects.subject_id
				AND students.id = student_class.student_id 
				AND student_class.student_id = '$student_id' 
				AND student_class.session = '$session'
				AND student_class.term = '$term'
				ORDER BY subjects.name
				";

			//echo $sql;
			if (!$this->mysql->execute_query($sql))
	            echo get_class($this)."-".$this->mysql->error;
	        else
	            return $this->mysql->fetch($this->mysql->result);
		}
		else
		{
			return array();
		}
	}
	
	function get_subject_grade($student_id, $subject_id, $session = null, $term = null)
	{
		if(is_null($session) and is_null($term))
		{
			$sess = new session();
			$sess_term = $sess->get_current_session_term();
			$session = $sess_term['session'];
			$term = $sess_term['term'];
		}
		
		$asmt = new assesment();
		$asmt_string = $asmt->assesment_string();
			
		$sql = "SELECT firstname, lastname, students.id, students.student_id, class_id, session, term, subjects.name, grades.*, ($asmt_string) as total
				FROM students, subjects, student_class  
				LEFT OUTER JOIN grades 
				ON  grades.session_id = student_class.session_id  
				WHERE grades.subject_id = '$subject_id'
				AND students.id = student_class.student_id 
				AND student_class.student_id = '$student_id' 
				AND student_class.session = '$session'
				AND student_class.term = '$term'
				ORDER BY subjects.name
				";

		//echo $sql;
		if (!$this->mysql->execute_query($sql))
            echo get_class($this)."-".$this->mysql->error;
        else
            return $this->mysql->fetch($this->mysql->result);
	}
	

	/**
	 * Gets an array of subjects offered by a student, takes the student id as its only parameter
	 * @param $student_id
	 * @return Array
	 */
	function get_subject_offered($student_id)
	{
		//class_subjects, teachers
        $session_id = $this->generate_session_id($student_id);
		
		$sql = "SELECT subjects.name
				FROM subjects, student_subjects 
				WHERE subjects.subject_id = student_subjects.subject_id
				AND student_subjects.session_id = '$session_id'  
				ORDER BY subjects.name
				";
		
		if (!$this->mysql->execute_query($sql))
            echo get_class($this)."-".$this->mysql->error;
        else
            return $this->mysql->fetch($this->mysql->result);
	}
	
	function search($keywords)
	{
		$search_results = array();
		//search keywords against student id, firstname and lastname
		foreach($keywords as $k => $v)
		{
			$sql = "SELECT * FROM `students` 
					WHERE student_id LIKE '$v%' 
					or firstname LIKE '$v%'
					or lastname LIKE '$v%'";
			
			//echo $sql; echo "<br>";
			if (!$this->mysql->execute_query($sql))
			{
            	echo get_class($this)."-".$this->mysql->error;
			}
        	else
        	{
            	while($row = @mysql_fetch_assoc($this->mysql->result))
                {
                    if(!in_array($row,$search_results))    
                		$search_results[] = $row;
                }
            
        	}
		}//end foreach
		//print_r($search_results);
		return $search_results;
	}
	
	/**
	 * This function checks whether a student is registered based on the current session
	 * 
	 * @param int student_id - student id
	 * @return string Yes|No
	 * */
	static function is_registered($student_id)
	{
		//becuase this function is a static function 
		//and php does not allow reference to its instance directly by using "$this" inside a static function
		//you have to explicitly create a student object
		$stud = new student();
		
		//regenerate student session id, to check against the database table
		$session_id = $stud->generate_session_id($student_id);
		
		$sql = "SELECT session_id 
				FROM student_class
				WHERE session_id = '$session_id'
				";
		if (!$stud->mysql->execute_query($sql))
		{
			echo get_class($stud)."-".$stud->mysql->error;
			exit();
		}
        else
        {
            //$result =  $stud->mysql->fetch($stud->mysql->result);
            if($stud->mysql->rows_affected() == 1)
				return "Yes";
			else
				return "No";
        }
		
		
	}
	
	
	function get_current_class($student_id)
	{
            $session_id = $this->generate_session_id($student_id);

            $sql = "SELECT class_id
                            FROM student_class
                            WHERE student_class.session_id = '$session_id';
                            ";

            if (!$this->mysql->execute_query($sql))
                echo get_class($this)."-".$this->mysql->error;
            else
            {
                $result =  $this->mysql->fetch($this->mysql->result);
                return $result[0]['class_id'];
            }
	}
	
	static function get_total_students()
    {
    	$stud = new student();
    	$row = $stud->select_all(array("id"),null,"WHERE status != 'expelled'");
    	return count($row);
    }

	static function get_total_boys()
    {
    	$stud = new student();
    	$row = $stud->select_all(array("id"),null,"WHERE status != 'expelled' AND gender='M'");
    	return count($row);
    }

	static function get_total_girls()
    {
    	$stud = new student();
    	$row = $stud->select_all(array("id"),null,"WHERE status != 'expelled' AND gender ='F' ");
    	return count($row);
    }	
    
    private function student_id_exists($student_id)
    {	
		$sql = "SELECT id 
				FROM students
				WHERE student_id = '$student_id'
				";
		
		if (!$this->mysql->execute_query($sql))
		{
            echo get_class($stud)."-".$stud->mysql->error;
            exit();
		}
        else
        {
            if($this->mysql->rows_affected() == 1)
				return true;
			else
				return false;
        }
    }
	
	function principal_comment($data)
	{
		$this->table_name = 'student_class';
		
		$data = parent::filter_input($data);
		
		$session_id = $data['session_id'];
		
		$qb = new queryBuilder();
	    $qb->set_table_name('student_class');
	    $qb->set_type("UPDATE");
	    $qb->set_where("WHERE session_id = '$session_id'");
	    $qb->prepare_data($data);
	    $sql = $qb->build_query();
		
		//echo $sql; exit;
		
		if (!$this->mysql->execute_query($sql))
            echo get_class($this)."-".$this->mysql->error;
		
	}
	
	function reset_password($student_id)
    {
        $password['password'] = md5('password');
        $this->update($student_id,$password);
    }


    function get_timetable($student_id)
	{
            $session_id = $this->generate_session_id($student_id);

            $sql = "SELECT timetable
                            FROM student_class, classes
                            WHERE student_class.session_id = '$session_id'
                            AND  student_class.class_id = classes.class_id
                            ";

            //echo $sql;
            if (!$this->mysql->execute_query($sql))
                echo get_class($this)."-".$this->mysql->error;
            else
            {
                $result =  $this->mysql->fetch($this->mysql->result);
                return $result[0]['timetable'];
            }
	}
}

?>
