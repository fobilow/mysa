<?php
class attendance extends Model
{
	var $table_name = "attendance";
	
	function insert($data)
	{
		$students = $data['student'];
		
		$std = new student();
		for($i = 0; $i < count($students); $i++)
		{
			$attendance['session_id'] = $std->generate_id($students[$i]);
			$attendance['subject_id'] = $data['subject_id'];
			$attendance['date'] = $data['date'];
			$attendance['entered_by'] = $_SESSION['userinfo']['firstname']." ".$_SESSION['userinfo']['lastname'];
			
			parent::insert($attendance);
			 
		}

                //this is where i put the student that always attends a class.
                //We need this student so that we can tell how many classes a student is supposed to attend.
                //i.e total number of classes(as in lectures)
	}
	
}