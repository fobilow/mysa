<?php
/**
 * @author FO
 * @version 1.0
 * @package thirdeye
 */
class event extends Model
{
	var $table_name = "calendar";
	
	var $primary_key = "event_id";
	
	
	function select_all(array $select_column = null, $start = null, $where=null)
    {
        $qb = new queryBuilder();
        $qb->set_table_name($this->get_table_name());
        $qb->set_type("SELECT");
        if (!is_null($select_column)) {
            $qb->set_field($select_column);
        }
        if (!is_null($start)) {
            $rpp = $this->get_record_per_page();
            $qb->set_limit("LIMIT $start,$rpp");
        }
        
     
	    $qb->set_where("WHERE event_date > NOW()");
	       
		$qb->set_order("ORDER BY event_date"); 
        $sql = $qb->build_query();
        //echo $sql;
        if (!$this->mysql->execute_query($sql))
            echo get_class($this)."-".$this->mysql->error;
        else
            return $this->mysql->fetch($this->mysql->result);
    }
}