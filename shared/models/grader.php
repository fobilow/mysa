<?php
class grader extends Model
{
	var $table_name = "gradingscheme";
	
	var $primary_key = "id";
	
	
	static function grade($score)
	{
		$me = new grader();
		$scheme = $me->select_all();
		for($i = 0; $i < count($scheme); $i++)
		{
			if($score >= $scheme[$i]['grade_min'] && $score <= $scheme[$i]['grade_max'])
				return $scheme[$i]['grade_letter'];
		}
	}
	
	function update_grader($grade_array)
	{
		for($i =0; $i < 5; $i++)
		{
			foreach($grade_array as $k => $v)
			{	
				$scheme[$k] = $v[$i];
				parent::update($scheme[$this->primary_key],$scheme);
			}
		}
		//return $scheme;
	}
}
