<?php
class thirdeye_parent extends Model
{
	var $table_name = 'parents';
	
	var $primary_key = 'parent_id';
	
	
	function update_by_email($id,$data)
	{
		$data = $this->filter_input($data);
		$qb = new queryBuilder();
		$qb->set_table_name($this->get_table_name());
		$qb->set_type("UPDATE");
		$qb->set_where("WHERE email = '$id'");
		$qb->prepare_data($data);
		$sql = $qb->build_query();
		//echo $sql;
		if (!$this->mysql->execute_query($sql))
			echo get_class($this)."-".$this->mysql->error;
	}
	
	function get_parents($name_like)
	{
		$value = mysql_escape_string($name_like);
		
		$sql = "SELECT parents.parent_id, parents.name, students.firstname, students.lastname
				FROM `parents` , `students` 
				WHERE students.parent_id = parents.parent_id
				AND parents.name LIKE '$value%'
				GROUP BY parents.parent_id
				LIMIT 0, 5
				";
		
		/*$sql = "SELECT name
				FROM `parents` 
				WHERE parents.name LIKE '$value%'
				LIMIT 0, 5
				";*/
		
		$this->mysql->execute_query($sql);
		//echo $sql;
		return $this->mysql->fetch($this->mysql->result);
	}
	
	function get_children($parent_id)
	{
		$sql = "SELECT students.id, students.firstname, students.picture
				FROM `parents` , `students` 
				WHERE students.parent_id = parents.parent_id
				AND parents.parent_id = $parent_id
				ORDER BY students.firstname
				";
		
		$this->mysql->execute_query($sql);
		return $this->mysql->fetch($this->mysql->result);
	}
	
	function reset_password($parent_id)
    {
    	$password['password'] = md5('password');
    	$this->update($parent_id,$password);
    }
    
	function search($keywords)
	{
		$search_results = array();
		//search keywords against student id, firstname and lastname
		foreach($keywords as $k => $v)
		{
			$sql = "SELECT * FROM `parents` 
					WHERE name LIKE '$v%'
					or email LIKE '$v%'";
			
			//echo $sql; echo "<br>";
			if (!$this->mysql->execute_query($sql))
			{
            	echo get_class($this)."-".$this->mysql->error;
			}
        	else
        	{
            	while($row = @mysql_fetch_assoc($this->mysql->result))
                {
                    if(!in_array($row,$search_results))    
                		$search_results[] = $row;
                }
            
        	}
		}//end foreach
		//print_r($search_results);
		return $search_results;
	}
}

?>