<?php
class message extends Model
{
	var $table_name = 'messages';
	
	var $primary_key = 'id';
	
	
	function sent($username,$usertype)
	{
		$sql = "SELECT * FROM $this->table_name
				WHERE messages.from = '$username'
				OR messages.to = 'Everybody'
				OR messages.to = '$usertype'
				ORDER BY datetime DESC";
		if (!$this->mysql->execute_query($sql))
            die(get_class($this)."-".$this->mysql->error);
        else
            return $this->mysql->fetch($this->mysql->result);
	}
	
	function inbox($username,$usertype)
	{
		$sql = "SELECT * FROM {$this->table_name}
				WHERE messages.to = '$username'
				OR messages.to = 'Everybody'
				OR messages.to = '$usertype'
				ORDER BY datetime DESC";
		if (!$this->mysql->execute_query($sql))
            die(get_class($this)."-".$this->mysql->error);
        else
            return $this->mysql->fetch($this->mysql->result);
	}
	
	function unread($username,$usertype)
	{
		$sql = "SELECT * FROM $this->table_name
				WHERE messages.status = 'unread'
				AND (
				messages.to = '$username'
				OR messages.to = 'Everybody'
				OR messages.to = '$usertype'
				)";
		if (!$this->mysql->execute_query($sql))
            die(get_class($this)."-".$this->mysql->error);
        else
            return count($this->mysql->fetch($this->mysql->result));
	}
	
	/*this function is read(present tense) not past tense :) */
	function read($msg_id)
	{
		//mark message as read and return message details
		$view_sql = "SELECT * FROM {$this->table_name} 
					 WHERE  id = '$msg_id'";
		
		$update_sql = "UPDATE $this->table_name SET `status` = 'read'
					   WHERE id = '$msg_id'";
		
		/* update */
		if (!$this->mysql->execute_query($update_sql))
            die(get_class($this)."-".$this->mysql->error);
            
		/* view */
        if (!$this->mysql->execute_query($view_sql))
            die(get_class($this)."-".$this->mysql->error);
        else
            return $this->mysql->fetch($this->mysql->result);  
	}
}

