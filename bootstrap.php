<?php
define("ROOTPATH",dirname('index.php'));

$model_path = ROOTPATH."/shared/models/";

$lib_path = ROOTPATH."/shared/lib/";

define("LIBPATH",$lib_path);

define("MODELPATH",$model_path);


function __autoload($class)
{
    $dir = array(LIBPATH,MODELPATH);
    $file_found = false;
    for($i = 0; $i < count($dir); $i++)
    {
		/*echo $dir[$i],"<br>";*/
		if(is_file($dir[$i]."$class.php"))
        {
			require_once($dir[$i]."$class.php");
            $file_found = true;
            break;
        }
    }  
    
    if(!$file_found)
    echo "Bootstrapper says: $class.php was not found";
}


?>