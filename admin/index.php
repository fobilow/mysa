<?php
ob_start("ob_gzhandler");

if(!isset($_SESSION))
{
	session_start();
}
if(!$_SESSION['admin_login'])
	header("Location: login.php");
	
require_once("bootstrap.php");
require_once("../config.php");
require_once("../shared/lib/common.php");

$school_name = host::load_db();

define("SCHOOL_NAME", $school_name);

$index_front = new indexController();
$index_front->run();


?>