<?php
require_once("bootstrap.php");
require_once("../config.php");

$school = host::start_app();

$value = trim($_GET['word']);
//$value = "T";
if(isset($value) and $value != '')
{
	$parent = new thirdeye_parent();
	$result = $parent->get_parents($value);
	
	$count = count($result);
	
	//echo $count;
	//print_r($result);
        //exit;
	$ajax = array();
	if($count > 0)
	{
		for($i = 0; $i < $count; $i++)
		{
			$item = $result[$i]['name'].' - '.$result[$i]['firstname'].' '.$result[$i]['lastname'];
			$ajax[$i]['id'] = $result[$i]['parent_id'];
			$ajax[$i]['value'] = $item;
			
		}
		
			header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
			header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
			header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
			header ("Pragma: no-cache"); // HTTP/1.0
			if (isset($_REQUEST['json']))
			{
				header("Content-Type: application/json");
				$data = array('results' => $ajax);
				echo json_encode($data);
			}
	}
	else
	{
		header("Content-Type: application/json");
		$data = array();
		echo json_encode($data);
	}
}

?>