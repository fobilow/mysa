<?php
if(!isset($_SESSION))
{
	session_start();
}
if(!$_SESSION['admin_login'])
	header("Location: login.php");
	
require_once("../shared/fpdf16/image_alpha.php");
require_once("../shared/lib/common.php");
require_once("bootstrap.php");
require_once("../config.php");

host::load_db();


$sch = new school(); // new school object
$student = new student();  //new student object
$schclass = new schoolClass();  //new school class object
$asm = new assesment(); //new assesment object


$school_info = $sch->select_all(array('school_name','school_logo','school_address','school_phoneno'));

$school_logo = $school_info[0]['school_logo'];
$school_name = $school_info[0]['school_name'];
$school_address = $school_info[0]['school_address'];
$school_phone = $school_info[0]['school_phoneno'];


$pdf = new PDF_ImageAlpha();

$pdf->SetMargins(20,20,20);

//Front cover of the report
$pdf->AddPage();
$pdf->SetFont('Arial','B',18);

$class = $_GET['class'];
$session = $_GET['session'];
$term = $_GET['term'];

$report_title = "Report Sheets for $class";
$sessionAndTerm = $session." ".to_sequence($term)." term";
 
if(!empty($school_logo))
	$pdf->Image($school_logo,90,40,35,40);

$pdf->ln(65);
$pdf->Cell(0,10,$school_name,'0',1,'C');
$pdf->Cell(0,10,$report_title,'0',1,'C');
$pdf->Cell(0,10,$sessionAndTerm,'0',1,'C');

$sg = new studentGrade();
$sg->position_in_subject($class,$session,$term); //compute and update student position in subject
$sg->position_in_class($class,$session,$term);

$students = $schclass->get_student_ids($class,$session,$term); //get student ids
$assesments = $asm->select_active(); //active assesments for current school
//echo count($students); exit;
for($i = 0; $i < count($students); $i++) 
{ 
	$id = $students[$i]['id'];
	
	//get student grades and display report sheet
	$data = $student->get_grades($id,$session,$term);
	if(count($data) != 0) //if we've got some data
	{

		$student_name = $data[0]['firstname']." ".$data[0]['lastname'];
		$student_id = $data[0]['student_id'];
		$student_class = $data[0]['class_id'];
		$student_gender = $data[0]['gender'];
		$session = $data[0]['session'];
		$term = $data[0]['term'];
		$class_position = $data[0]['class_position'];
		
		//one page per student report
		$pdf->AddPage();
		if(!empty($school_logo))
		{
			$pdf->Image($school_logo,20,20,23,25); //school logo
		}
		$pdf->Cell(25);
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(0,5,$school_name,0,1); //write school name
		$pdf->Cell(25);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(0,5,$school_address,0,1); //write school address
		$pdf->Cell(25);
		$pdf->Cell(0,5,"Tel: ".$school_phone,0,1); //write school phone no.
		
		$pdf->ln(15);
		
		$pdf->SetFont('Arial','B',8); $pdf->Cell(30,5,'Student Name',1); 
		
		$pdf->SetFont('Arial','',8); $pdf->Cell(60,5,$student_name,1);
		
		$pdf->SetFont('Arial','B',8); $pdf->Cell(25,5,'Sex',1); 
		
		$pdf->SetFont('Arial','',8); $pdf->Cell(0,5,$student_gender,1,1);
		
		$pdf->SetFont('Arial','B',8); $pdf->Cell(30,5,'Student ID',1); 
		
		$pdf->SetFont('Arial','',8); $pdf->Cell(60,5,$student_id,1);
		
		$pdf->SetFont('Arial','B',8); $pdf->Cell(25,5,'Student Class',1); 
		
		$pdf->SetFont('Arial','',8); $pdf->Cell(0,5,$student_class,1,1);
		
		$pdf->ln();
		
		$pdf->SetFont('Arial','B',8); $pdf->Cell(30,5,'Average Total',1); 
		$pdf->SetFont('Arial','', 8); $pdf->Cell(60,5,$avg_total,1);
		
		$pdf->SetFont('Arial','B',8); $pdf->Cell(25,5,'Position in Class',1); 
		$pdf->SetFont('Arial','',12); $pdf->Cell(0,5,$sg->position_text($class_position),1,1);
		
		$pdf->ln();
		
		//Headers for student grades
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(50,5,'Subject Name',1);
		$w2 = array();
		foreach($assesments as $assesment)
		{
			$w = $pdf->GetStringWidth($assesment['assesment_name']) + 3;
		
			$pdf->Cell($w,5,$assesment['assesment_name'],1,0,'C'); 
			
			$w2[] = $w; //store the width in an array to use later
		}
		$pdf->Cell(10,5,'Total',1,0,'C');
		$pdf->Cell(10,5,'Grade',1,0,'C');
		$pdf->Cell(15,5,'Position',1,0,'C');
		$pdf->Cell(0,5,'Remark',1,1);
		
		//grade rows
		foreach($data as $row)
		{
			$pdf->SetFont('Arial','',8);
			
			$name = ucwords(strtolower($row['name']));
			$pdf->Cell(50,5,$name,1);
			$r = 0;
			foreach($assesments as $assesment)
			{
				$score = $row[$assesment['real_name']];
				$pdf->Cell($w2[$r],5,$score,1,0,'C');
				$r++; 
			}
			$pdf->Cell(10,5,$row['total'],1,0,'C');
			$pdf->Cell(10,5,grader::grade($row['total']),1,0,'C');
			$pdf->Cell(15,5,$sg->position_text($row['position']),1,0,'C');
			$pdf->Cell(0,5,'',1,1);
		}
		
		$pdf->ln();
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(40,7,"Principal's comment",0,1);
		
		$pdf->SetFont('Arial','',12);
		$pdf->Cell(0,5,' ','B',1);
		$pdf->Cell(0,5,' ','B',1);
		
	}
	
	
}

$pdf->Output();

?>