<?php
class settingsController extends Controller
{
	
	function index()
	{
		
		$school_obj = new school();
		
		$db_theme = $school_obj->select_all(array('theme'));
		$sch_name = $school_obj->select_all(array('school_name','school_logo'));
		
		//theme info
		$themes = $this->get_themes();
		
		//grading scheme info
		$grader_obj = new grader();
		$scheme = $grader_obj->select_all();
		
		//assesment info
		$assesment_obj = new assesment();
		$ass = $assesment_obj->select_all();

		//session info
		$session_obj = new session();
		$sess = $session_obj->select_all();
		
		
		include(VIEWPATH."customize.php");
		//$this->set_session();
	}
	
	function set_theme()
	{
		$school_obj = new school();
		$sch_info = $school_obj->select_all(array('school_name'));
		$_SESSION['theme'] = $_POST['theme'];
		
		$school_obj->update($sch_info[0]['school_name'],$_POST);
		
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");	
	}
	
	function get_themes()
	{
		$theme_dir = "../css/themes";
		//check that theme directrory exists
		if(is_dir($theme_dir))
		{
			//if it exists, get all the files in the directory
			$theme_files = scandir($theme_dir);
			
			$c = count($theme_files);
			if($c > 2) //make sure we have more than . and ..
			{
				for($i = 0; $i < $c; $i++)
				{
					if($theme_files[$i] != "." && $theme_files[$i] != "..")
					{
						
						$pos = strrpos($theme_files[$i],".");
		
						if($pos !== false)
						{
							$themes[] =  substr($theme_files[$i],0,$pos);
						}
					}	
				}
				sort($themes);
				return $themes; //return the files sorted by name	
			}
			else
			{
				$themes[] = "No theme available";
				return $themes;
			}
		}
		
		
	}
	
	
	function update_grader()
	{
		
		$grader_obj = new grader();
		$grader_obj->update_grader($_POST);
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
	}
	

	function add_assesment()
	{
		$assesment_obj = new assesment();
		$assesment_obj->insert($_POST);
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
	}
	
	
	function activate()
	{
		$id = $_GET['id'];
		$assesment_obj = new assesment();
		$status = array('status' => 'Active');
		$assesment_obj->update($id,$status);
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
	}
	
	function deactivate()
	{
		$id = $_GET['id'];
		$assesment_obj = new assesment();
		$status = array('status' => 'Inactive');
		$assesment_obj->update($id,$status);
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
	}
	
	//set session function
	function set_session()
	{

		//$this->xray($_POST); exit;
		$session_obj = new session();

		$months_from = $_POST['month_from'];
		$months_to = $_POST['month_to'];
		$terms = $_POST['term'];

		for($i = 0; $i < count($terms); $i++)
		{		
			$data['month_from'] = $months_from[$i];
			$data['month_to'] = $months_to[$i];
			$data['term'] = $terms[$i];
			$session_obj->insert($data);
		}
		
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
		
	}

	function edit_grading_scheme()
	{
		//grading scheme info
		$grader_obj = new grader();
		$scheme = $grader_obj->select_all();
		include(VIEWPATH."edit_grading_scheme.php");
	}
	
	//upload school logo
	function upload_logo()
	{
		//$this->xray($_POST); exit;
		if($_FILES['school_logo']['name'] != "")
		{
			$fu = new fileUpload();
			$fu->add_file('school_logo','image','../shared/uploads/images/',SCHOOL_NAME."_logo");
			$fu->validate();

			if(!$fu->upload())
			{
				$errors = $fu->error;
				$this->index();
				echo formPopulator::populate();
			}
			else
			{
				$sch = new school();
				$sch->update_logo($_POST);
				header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
			}
		}
	
	}

	
	
	
	
	
}

?>
