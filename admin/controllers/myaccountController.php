<?php
class myaccountController extends Controller
{
	
	function index()
	{
		//include(VIEWPATH."myaccount.php");
		$this->school_info();
	}
	
	function school_info()
	{
		//this is the place, where i hook up to the account db outside the school db
		
		$school_obj = new school();
		$account_info = $school_obj->get_account();
		
		include(VIEWPATH."school_info.php");
	}
	
	function update_school()
	{
       $sch = new school();
		
		$val = new validation($_POST);
		$notempty = array('school_name','school_admin', 'school_email','school_phoneno','school_address');
		$val->is_empty($notempty);
		$val->is_email('school_email');
		$val->is_number('school_phoneno');
		if($val->error_found())
		{
			$errors = $val->get_error_messages();
			$sch_info = $sch->select_all();
			include(VIEWPATH."school_info.php");
			echo formPopulator::populate();
		}
		else
		{
			$sch->insert($_POST); // this line updates school information, if it already exists, might change the name of the function later
			
			$sch_info = $sch->select_all();
			include(VIEWPATH."school_info.php");
		}
	}
	
	
	
	function change_username()
	{
		if(!isset($_POST['Submit']))
		{
			include(VIEWPATH."change_username.php");
		}
		else
		{
			$old_username = $_POST['old_username'];
                        $new_username = $_POST['new_username'];
                        $password = $_POST['password'];

                        $login = new login($old_username,$password);
                        $login->set_table("admins");
                        if($login->authenticate())
                        {
                            $new = array('username' => $new_username);
                            $admin_obj = new admin();
                            $admin_obj->update($old_username,$new);
                            $msg = "Your changes were made successfully. Changes will take effect next time you login";
                            include(VIEWPATH."change_username.php");
                            //inform user that changes will apply when next they login
                        }
                        else
                        {
                            $msg = "There is an error in the password supplied";
                            include(VIEWPATH."change_username.php");
                        }
		}
	}
	
	function change_password()
	{
		if(!isset($_POST['Submit']))
		{
			include(VIEWPATH."change_password.php");
		}
		else
		{
                    $user_obj = new user();
                    $user_obj->change_password("admins");
		}
	}
}

?>
