<?php
class teacherController extends Controller
{    
	function index()
	{
		$teacher_obj = new teacher();
		$stafflist = $teacher_obj->select_all();
		include(VIEWPATH."view_teachers.php");
	}
	
	function add_teacher()
	{
		if(!isset($_POST['Submit']))
		{
			include(VIEWPATH."add_teacher.php");
		}
		else
		{
			$val = new validation($_POST);
			$notempty = array('teacher_id','firstname','lastname');
			$val->is_empty($notempty);
			if(!empty($_POST['email']))
				$val->is_email('email');
			if($val->error_found())
			{
				$errors = $val->get_error_messages();
				include(VIEWPATH."add_teacher.php");
				echo formPopulator::populate();
			}
			else
			{
				$this->upload_teacher_pic();
				$_POST['password'] = md5('password');
			
				$teacher_obj = new teacher();
				$teacher_obj->insert($_POST);
				
				$_SESSION['sysmsg'] = "<div id=\"msg\">You have sucessfully added {$_POST['firstname']} {$_POST['lastname']}</div>";
				$_SESSION['msgshown'] = false;
				header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}?tab=teacher&action=view");
			}
		}
	}
	
	function view()
	{
		$teacher_obj = new teacher();
		$stafflist = $teacher_obj->select_all();
		include(VIEWPATH."view_teachers.php");
	}

    function confirm_delete_teacher()
	{
		$teacher_id = $_GET['teacher_id'];
		$teacher_obj = new teacher();
		$teacher = $teacher_obj->select($teacher_id);
		
		$include_page = "confirm_delete_teacher.php";
		include(VIEWPATH."teacher_profile.php");
	}

	function delete_teacher()
	{
		$teachers = $_POST['teacher_id'];
		$teacher_obj = new teacher();
		for($i = 0; $i < count($teachers); $i++)
		{
			$teacher_obj->delete($teachers[$i]);
		}
		
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}");
	}
	
	function edit_teacher()
	{
		if(isset($_POST['teacher_id']))
			$teacher_id = $_POST['teacher_id'][0];
		if(isset($_GET['teacher_id']))
			$teacher_id = $_GET['teacher_id'];
	
		$teacher_obj = new teacher();
		$teacher = $teacher_obj->select($teacher_id);
		
		$include_page = "edit_teacher.php";
		include(VIEWPATH."teacher_profile.php");
	}
	
	function update_teacher()
	{
		$this->upload_teacher_pic();
		
		$id = $_POST['id'];
			
		$teacher_obj = new teacher();
		$teacher_obj->update($id,$_POST);
		
		
		header("Location: http://{$_SERVER['SERVER_NAME']}{$_SERVER['SCRIPT_NAME']}?action=teacher_profile&teacher_id=$id");
	}
	
	function teacher_profile()
	{
		$id = $_GET['id'];
		if(isset($_GET['page']))
		{
			$page = $_GET['page'];
			$this->$page();
		}
		else
		{
			$this->_teacher_profile();
		}
	}
	
	function _teacher_profile()
	{
		$id = $_GET['teacher_id'];
		$teacher_obj = new teacher();
		
		$teacher = $teacher_obj->select($id);
		$subjects = $teacher_obj->get_subjects_taught($id);
		
		$session_obj = new session();
		$session = $session_obj->get_current_session_term();
		
		$include_page = "tprofile.php";
		include(VIEWPATH."teacher_profile.php");
	}
	
	function reset_password()
	{
		$teacher_id = $_GET['teacher_id'];
		$teacher_obj = new teacher();
		$teacher_obj->reset_password($teacher_id);
		$teacher = $teacher_obj->select($teacher_id);
		
		$msg = "<p>You have succesfully reset teachers password  </p>
		        <p>Teacher's password is now <strong>password</strong> <br />
		        </p>
        		<a href =\"teachers.php\">Continue</a>";
		
		$include_page = "password_reset.php";
		include(VIEWPATH."teacher_profile.php");
	}
	
	function upload_teacher_pic()
	{
            if($_FILES['picture']['name'] != "")
            {
                $fu = new fileUpload();
                $fu->add_file('picture','image','../shared/uploads/images/',true);
                $fu->validate();
                return $fu->upload();
            }
	}
}
?>