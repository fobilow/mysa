<?php
include_once("bootstrap.php");
include("../config.php");
include_once("../shared/lib/common.php"); 

host::load_db();

$keywords = get_keywords();
//$this->xray($keywords); exit;

$teacher_obj = new teacher();
$stafflist = $teacher_obj->search($keywords);
?>
<table width="98%" border="0" align="center" cellpadding="5" cellspacing="0">
      <tr class="table-header">
        <td width="36%">Name</td>
        <td width="64%">ID</td>
      </tr>
	  <?php
	  for($i = 0; $i < count($stafflist); $i++)
	  { ?>
      <tr bgcolor="<?php echo color($i); ?>">
        <td><a href="teachers.php?action=teacher_profile&teacher_id=<?php echo $stafflist[$i]['id']; ?>"><?php echo $stafflist[$i]['firstname'],' ',$stafflist[$i]['lastname']; ?></a></td>
        <td width="80%"><a href="teachers.php?action=teacher_profile&teacher_id=<?php echo $stafflist[$i]['id']; ?>"><?php echo $stafflist[$i]['teacher_id']; ?></a></td>
      </tr>
	  <?php } ?>
    </table>