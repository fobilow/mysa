<?php include('includes/header.php'); ?>
              <h1>New Student</h1>
            	<div id="studentTable">
               		<form action="students.php?action=add_student" method="post" enctype="multipart/form-data" name="add_student" id="add_student">
       

                              <table width="100%" border="0" cellpadding="3" cellspacing="0" >
                              <tr>
                                <td width="150"> Photo:</td>
                                <td><label><input name="picture" type="file" id="picture" size="37" /></label></td>
                              </tr>
                              <tr>
                                <td width="150"> Gender: </td>
                                <td><input name="gender" type="radio" value="M" id="M" />male<input name="gender" type="radio" value="F" id="F" />
                                  female
                                  <label class="error"><?php echo $errors['gender']; ?></label></td>
                              </tr>
                              <tr>
                                <td width="150">Student ID: </td>
                                <td><label class="error">
                                  <input name="student_id" type="text" id="student_id" size="50" value="" />
                                  <?php echo $errors['id']; ?></label></td>
                              </tr>
                              <tr>
                                <td width="150"> First name:</td>
                                <td><label class="error">
                                  <input name="firstname" type="text" id="firstname" size="50"
                                value="" />
                                  <?php echo $errors['firstname']; ?></label></td>
                              </tr>
                              <tr>
                                <td width="150">Last name:</td>
                                <td><label class="error">
                                  <input name="lastname" type="text" id="lastname" size="50"
                                value="" />
                                  <?php echo $errors['lastname']; ?></label></td>
                              </tr>
                              <tr>
                                <td width="150"> Date of birth:</td>
                                <td><label for="select"></label>
                                  <select name="dob_d" id="dob_d">
                                  <?php for($i = 1; $i <= 31; $i++): ?>
                                  	<option value="<?php echo str_pad($i,2,'0',0); ?>"><?php echo str_pad($i,2,'0',0); ?></option>
                                  <?php endfor; ?>
                                  </select>
                                  <select name="dob_m" id="dob_m">
                                  	<option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                  </select>
                                  <select name="dob_y" id="dob_y">
                                  <?php for($i=1980; $i <= date('Y'); $i++): ?>
                                  	<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                  <?php endfor; ?>
                                  </select>
                                  <?php echo $errors['dob']; ?></td>
                              </tr>
                              <tr>
                                <td width="150"> Email address:</td>
                                <td><label class="error">
                                  <input name="email" type="text" id="email" size="50" value="" />
                                  <?php echo $errors['email']; ?></label></td>
                              </tr>
                              <tr>
                                <td width="150"> Phone number:</td>
                                <td><label class="error">
                                  <input name="phoneno" type="text" id="phoneno" size="50"
                                value="" />
                                  <?php echo $errors['phoneno']; ?></label></td>
                              </tr>
                              <tr>
                                <td width="150"> Status:</td>
                                <td><label>
                                  <select name="status" id="status">
                                    <option value="enrolled" selected="selected">Enrolled</option>
                                    <option value="expelled">Expelled</option>
                                    <option value="graduated">Graduated</option>
                                    <option value="suspended">Suspended</option>
                                    <option value="transfered">Transfered</option>
                                  </select>
                                </label></td>
                              </tr>
                              <tr>
                                <td width="150">&nbsp;</td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td colspan="2">Parent Information </td>
                              </tr>
                              <tr>
                                <td colspan="2"><label><input type="checkbox" name="checkbox" value="checkbox" id="pcheck" onclick="showAutocomplete()" />
                                  I have entered this parent information before</label></td>
                              </tr>
                              </table>


                                <table width="100%" border="0" cellpadding="3" cellspacing="0" id="newparent" >
                                  <tr>
                                    <td width="150">Name:</td>
                                    <td><label>
                                      <input name="name" type="text" id="name" size="50" />
                                    </label></td>
                                  </tr>
                                  <tr>
                                    <td width="150">Email:</td>
                                    <td><label>
                                      <input name="email" type="text" id="email" size="50" />
                                    </label></td>
                                  </tr>
                                  <tr>
                                    <td width="150">Mobile Phone: </td>
                                    <td><label>
                                      <input name="mobile_phone" type="text" id="mobile_phone" size="50" />
                                    </label></td>
                                  </tr>
                                  <tr>
                                    <td width="150">Land Phone: </td>
                                    <td><label>
                                      <input name="land_phone" type="text" id="land_phone" size="50" />
                                    </label></td>
                                  </tr>
                                  <tr>
                                    <td width="150">Address:</td>
                                    <td><label>
                                      <textarea name="address" cols="50" rows="5" id="address"></textarea>
                                    </label></td>
                                  </tr>
                              </table>

                              <table width="100%" border="0" cellpadding="5" cellspacing="0" id="existingparent" style="display:none;">
                       			    <tr>
                                    <td width="">Select Parent </td>
                                    <td width=""><label>
                                      <select name="parent_id" id="parent_id">
                                      <?php for($i = 0; $i < count($result); $i++): ?>
                                      <option value="<?php echo $result[$i]['parent_id']; ?>"><?php echo $result[$i]['name']; ?></option>
                                      <?php endfor; ?>
                                    </select>
                                  </label></td>
                                </tr>	
                              </table>


                            <table width="100%" border="0" cellpadding="3" cellspacing="0" >
                       	      <tr>
                                <td width="150">&nbsp;</td>
                                <td><input type="submit" name="Submit" value="Submit" /></td>
                              </tr>
                            </table>


                  </form>
            	</div>
            </div> <!-- end of main -->



            <div class="sidebar right">
            	<div class="box rounded">
                	<span class="title">What would you like to do?</span>
                    <span class="hint">Click on what you want to do to begin</span>
                    <div><a href="students.php?action=add_student">Add a New Student</a></div>
                    <div><a href="#">Register a Student</a></div>
                    <div>Search for a Student</div>
                    <div><input type="text" style="width:193px; height:25px;" id="search_string" onkeyup="searchStudent();"/></div>
                </div>
            </div>
            <div style="clear:both"></div>
        </div>
    </div>	
</body>
</html>
