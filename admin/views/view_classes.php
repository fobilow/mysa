<?php include('includes/header.php'); ?>
  <h1>Classes</h1>
<form name="classlist" method="post" action="classes.php?action=add_class">
    <table width="98%" border="0" cellpadding="5" cellspacing="0">
      <tr class="table-header">
        <td width="2%"></th>
        <td width="17%">Class</td>
        <td width="31%">Teacher</td>
        <td width="28%">No. of students</td>
      </tr>
       <tr bgcolor="#FFFF99">
        <td>&nbsp;</td>
        <td><input name="class_id" type="text" id="class_id" /></td>
        <td><span class="error">
          <select name="teacher_id" id="teacher_id">
            <option value="" selected="selected">...select...</option>
            <?php
        for($i = 0; $i < count($teacher_list); $i++)
        {
            echo "<option value=\"{$teacher_list[$i]['id']}\">{$teacher_list[$i]['firstname']} {$teacher_list[$i]['lastname']}</option>";
        }
        ?>
          </select>
          <input type="submit" name="Submit" value="Add"/>
        </span></td>
        <td>&nbsp;</td>
      </tr>
      <?php
      for($i = 0; $i < count($class_list); $i++)
      { ?>
      <tr bgcolor="<?php echo color($i); ?>">
        <td><label>
          <input type="checkbox" name="class_id[]" value="<?php echo $class_list[$i]['class_id']; ?>" 
          onclick="toggle_edit_delete('class_id[]','edit','delete')"/>
        </label></td>
        <td><a href="classes.php?action=class_subjects&amp;class=<?php echo $class_list[$i]['class_id']; ?>"><?php echo $class_list[$i]['class_id']; ?></a></td>
        <td><?php echo $class_list[$i]['firstname']." ".$class_list[$i]['lastname']; ?></td>
        <td><?php echo $class_obj->get_student_count($class_list[$i]['class_id']); ?></td>
      </tr>
      <?php } ?>
      <tr bgcolor="<?php echo color($i); ?>">
        <td colspan="4"><input type="submit" name="editAction" value="Edit..." id="edit" disabled="disabled"
        onclick="document.classlist.action='classes.php?action=edit_class';" /></td>
      </tr>
    </table>
</form>
</div>


            <div class="sidebar right">
            	<div class="box rounded">
                	<span class="title">Hint &amp; Tips</span>
                    <div class="hint">To add a class, use the form on the first row of the table in the left side of the page</div>
                  	<div class="hint">To change a subject's teacher or allocate subjects to a class, click on the class name on the list in the left side of this page</div>
                    <div class="hint">You can assign teacher to teach multiple subjects to multiple classes, so take advantage of that.</div>
                </div>
            </div>
            <div style="clear:both"></div>

