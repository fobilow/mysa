
    	<?php include('includes/header.php'); ?>


            	<h1>Customize MySchoolAssist </h1>
           		<div class="customize">
                 <form action="customize.php?action=upload_logo" method="post" enctype="multipart/form-data" name="lookAndFeel">
                 <h3>Look &amp; Feel </h3>
                 <p>Change the way MySchoolAssist looks to fit to your existing school colour </p>
                 <table width="100%" border="0" cellpadding="5" cellspacing="0">
                      <tr>
                        <td width="15%" valign="top">School logo: </td>
                        <td width="85%">
                        <img src="<?php echo $sch_name[0]['school_logo']; ?>" width="150" height="100" /></td>
                      </tr>
                      <tr>
                        <td valign="top">&nbsp;</td>
                        <td><input name="school_logo" type="file" size="20" onchange="document.getElementById('uploadlogo').disabled = false" />
                          <label class="error"><?php echo $errors['school_logo']; ?></label>
                          <input name="school_name" type="hidden" id="school_name" value="<?php echo stripcslashes($sch_name[0]['school_name']); ?>" />
<input name="uploadlogo" type="submit" disabled="disabled" value="Upload Logo" id="uploadlogo" /></td>
                      </tr>
                      <tr>
                        <td valign="top">Select a theme</td>
                        <td><select name="theme" id="theme" onchange="applyTheme()">
                          <?php for($i = 0; $i < count($themes); $i++)
                            { 
                                $theme = "../css/themes/".$themes[$i].".css";
                            ?>
                          <option value="<?php echo $theme; ?>" <?php if($theme == $db_theme[0]['theme']) echo 'selected'; ?>>
                          <?php echo $themes[$i]; ?>		  </option>
                          <?php } ?>
                        </select></td>
                      </tr>
                    </table>
                  </form>
                </div>
                 <!-- form 3 is grading scheme form -->
                 <div class="customize" id="gradescheme">
                    <h3>Grading Scheme</h3>
                    <p>Grading scheme is a setting that allows you to define how you grade your students. MySchoolAssist comes with a default grading scheme, you can modifiy this scheme by clicking the <a href="#" onclick="editGradeScheme()">edit</a> link. </p>
                    <table width="70%" border="0" cellpadding="5" cellspacing="0">
                        <tr>
                          <td width="13%"><strong>Grade</strong></td>
                          <td width="24%"><strong>From</strong></td>
                          <td width="63%"><strong>To</strong></td>
                        </tr>
                        <?php
                          for($i = 0; $i < count($scheme); $i++)
                          {
                          ?>
                                <tr bgcolor="<?php echo color($i); ?>">
                                  <td><strong><?php echo $scheme[$i]['grade_letter']; ?></strong></td>
                                  <td><?php echo $scheme[$i]['grade_min']; ?></td>
                                  <td><?php echo $scheme[$i]['grade_max']; ?></td>
                                </tr>
                                <?php 
                          } 
                          ?>
                        <tr>
                          <td colspan="2"></td>
                          <td align="right"><a href="#" onclick="editGradeScheme()">Edit</a></td>
                        </tr>
                    </table>
                </div>
  <!--- form 4 is assesment form -->
<div class="customize" id="assesmentdiv">
  <form action="customize.php?action=add_assesment#assesmentdiv" method="post" name="set_assesment" id="">
    <h3>School Assesments  </h3>
            <p><a href="#assesmentdiv" onclick="$('#newAss').show();">Add new assesment</a></p>
            <table width="70%" border="0" cellpadding="5" cellspacing="0">
              <tr>
                <td width="31%"><strong>Assesment Name</strong></td>
                <td width="25%"><strong>Max. Score</strong></td>
                <td width="44%">&nbsp;</td>
              </tr>
              <tr bgcolor="<?php echo color($i); ?>" id="newAss" style="display:none; background-color:#FFFF99;">
                <td width="31%"><input type="text" name="assesment_name" id="new" /></td>
                <td width="25%"><input name="max_score" type="text" size="3" maxlength="3" /></td>
                <td width="44%">
                  <input name="Submit" type="submit" value="Add" />
                <a href="#assesmentdiv" onclick="$('#newAss').hide()">cancel</a></td>
              </tr>
              <?php
	  for($i = 0; $i < count($ass); $i++)
	  {
	  		$status = $ass[$i]['status'];
			$id = 'ass'.$ass[$i]['assesment_id'];
	  ?>
              <tr bgcolor="<?php echo color($i); ?>" id="<?php echo $id; ?>">
                <td width="31%"><a href="#assesmentdiv" onclick="showEdit('<?php echo $id; ?>','<?php echo $ass[$i]['assesment_id']; ?>','<?php echo $ass[$i]['assesment_name']; ?>','<?php echo $ass[$i]['max_score']; ?>')"><?php echo $ass[$i]['assesment_name']; ?></a> </td>
                <td width="25%"><?php echo $ass[$i]['max_score']; ?></td>
                <td width="44%"><?php if($status == 'Active') { ?>
                    <a href="customize.php?action=deactivate&amp;id=<?php echo $ass[$i]['assesment_id']; ?>">deactivate</a>
                    <?php } else { ?>
                    <a href="customize.php?action=activate&amp;id=<?php echo $ass[$i]['assesment_id']; ?>">activate</a>
                    <?php } ?>                </td>
              </tr>
              <?php 
	  } 
	  ?>
      </table>
    <div id="aaa"></div>
  </form>
  <p>&nbsp;</p>
</div>

<div class="customize" id="sessiontermdiv">
 <form action="customize.php?action=set_session" method="post">
 <h3><br />
   School Session/Term </h3>
 <p>Customize your school session/term so that MySchoolAssist can know what session/term your school is in at anytime.</p>
 <table width="50%" border="0" cellpadding="5" cellspacing="0">
      <tr>
        <td width="21%"><strong>Term</strong></td>
        <td width="79%"><strong>Period (from - to) </strong></td>
      </tr>
      <tr>
        <td>1
          <input name="term[]" type="hidden" id="term[]" value="1" /></td>
        <td>
          <select name="month_from[]" id="month_from[]">
            <option value="1" <?php if ($sess[0]['month_from'] == 1) {echo "selected=\"selected\"";} ?>>Jan</option>
            <option value="2" <?php if ($sess[0]['month_from'] == 2) {echo "selected=\"selected\"";} ?>>Feb</option>
            <option value="3" <?php if ($sess[0]['month_from'] == 3) {echo "selected=\"selected\"";} ?>>Mar</option>
            <option value="4" <?php if ($sess[0]['month_from'] == 4) {echo "selected=\"selected\"";} ?>>Apr</option>
            <option value="5" <?php if ($sess[0]['month_from'] == 5) {echo "selected=\"selected\"";} ?>>May</option>
            <option value="6" <?php if ($sess[0]['month_from'] == 6) {echo "selected=\"selected\"";} ?>>Jun</option>
            <option value="7" <?php if ($sess[0]['month_from'] == 7) {echo "selected=\"selected\"";} ?>>Jul</option>
            <option value="8" <?php if ($sess[0]['month_from'] == 8) {echo "selected=\"selected\"";} ?>>Aug</option>
            <option value="9" <?php if ($sess[0]['month_from'] == 9) {echo "selected=\"selected\"";} ?>>Sep</option>
            <option value="10" <?php if ($sess[0]['month_from'] == 10) {echo "selected=\"selected\"";} ?>>Oct</option>
            <option value="11" <?php if ($sess[0]['month_from'] == 11) {echo "selected=\"selected\"";} ?>>Nov</option>
            <option value="12" <?php if ($sess[0]['month_from'] == 12) {echo "selected=\"selected\"";} ?>>Dec</option>
          </select>
        -
        <select name="month_to[]" id="month_to[]">
          <option value="1" <?php if ($sess[0]['month_to'] == 1) {echo "selected=\"selected\"";} ?>>Jan</option>
          <option value="2" <?php if ($sess[0]['month_to'] == 2) {echo "selected=\"selected\"";} ?>>Feb</option>
          <option value="3" <?php if ($sess[0]['month_to'] == 3) {echo "selected=\"selected\"";} ?>>Mar</option>
          <option value="4" <?php if ($sess[0]['month_to'] == 4) {echo "selected=\"selected\"";} ?>>Apr</option>
          <option value="5" <?php if ($sess[0]['month_to'] == 5) {echo "selected=\"selected\"";} ?>>May</option>
          <option value="6" <?php if ($sess[0]['month_to'] == 6) {echo "selected=\"selected\"";} ?>>Jun</option>
          <option value="7" <?php if ($sess[0]['month_to'] == 7) {echo "selected=\"selected\"";} ?>>Jul</option>
          <option value="8" <?php if ($sess[0]['month_to'] == 8) {echo "selected=\"selected\"";} ?>>Aug</option>
          <option value="9" <?php if ($sess[0]['month_to'] == 9) {echo "selected=\"selected\"";} ?>>Sep</option>
          <option value="10" <?php if ($sess[0]['month_to'] == 10) {echo "selected=\"selected\"";} ?>>Oct</option>
          <option value="11" <?php if ($sess[0]['month_to'] == 11) {echo "selected=\"selected\"";} ?>>Nov</option>
          <option value="12" <?php if ($sess[0]['month_to'] == 12) {echo "selected=\"selected\"";} ?>>Dec</option>
        </select>
       </td>
      </tr>
      <tr>
        <td>2 
          <input name="term[]" type="hidden" id="term[]" value="2" /></td>
        <td><select name="month_from[]" id="month_from[]">
            <option value="1" <?php if ($sess[1]['month_from'] == 1) {echo "selected=\"selected\"";} ?>>Jan</option>
            <option value="2" <?php if ($sess[1]['month_from'] == 2) {echo "selected=\"selected\"";} ?>>Feb</option>
            <option value="3" <?php if ($sess[1]['month_from'] == 3) {echo "selected=\"selected\"";} ?>>Mar</option>
            <option value="4" <?php if ($sess[1]['month_from'] == 4) {echo "selected=\"selected\"";} ?>>Apr</option>
            <option value="5" <?php if ($sess[1]['month_from'] == 5) {echo "selected=\"selected\"";} ?>>May</option>
            <option value="6" <?php if ($sess[1]['month_from'] == 6) {echo "selected=\"selected\"";} ?>>Jun</option>
            <option value="7" <?php if ($sess[1]['month_from'] == 7) {echo "selected=\"selected\"";} ?>>Jul</option>
            <option value="8" <?php if ($sess[1]['month_from'] == 8) {echo "selected=\"selected\"";} ?>>Aug</option>
            <option value="9" <?php if ($sess[1]['month_from'] == 9) {echo "selected=\"selected\"";} ?>>Sep</option>
            <option value="10" <?php if ($sess[1]['month_from'] == 10) {echo "selected=\"selected\"";} ?>>Oct</option>
            <option value="11" <?php if ($sess[1]['month_from'] == 11) {echo "selected=\"selected\"";} ?>>Nov</option>
            <option value="12" <?php if ($sess[1]['month_from'] == 12) {echo "selected=\"selected\"";} ?>>Dec</option>
          </select>
        -
        <select name="month_to[]" id="month_to[]">
          <option value="1" <?php if ($sess[1]['month_to'] == 1) {echo "selected=\"selected\"";} ?>>Jan</option>
          <option value="2" <?php if ($sess[1]['month_to'] == 2) {echo "selected=\"selected\"";} ?>>Feb</option>
          <option value="3" <?php if ($sess[1]['month_to'] == 3) {echo "selected=\"selected\"";} ?>>Mar</option>
          <option value="4" <?php if ($sess[1]['month_to'] == 4) {echo "selected=\"selected\"";} ?>>Apr</option>
          <option value="5" <?php if ($sess[1]['month_to'] == 5) {echo "selected=\"selected\"";} ?>>May</option>
          <option value="6" <?php if ($sess[1]['month_to'] == 6) {echo "selected=\"selected\"";} ?>>Jun</option>
          <option value="7" <?php if ($sess[1]['month_to'] == 7) {echo "selected=\"selected\"";} ?>>Jul</option>
          <option value="8" <?php if ($sess[1]['month_to'] == 8) {echo "selected=\"selected\"";} ?>>Aug</option>
          <option value="9" <?php if ($sess[1]['month_to'] == 9) {echo "selected=\"selected\"";} ?>>Sep</option>
          <option value="10" <?php if ($sess[1]['month_to'] == 10) {echo "selected=\"selected\"";} ?>>Oct</option>
          <option value="11" <?php if ($sess[1]['month_to'] == 11) {echo "selected=\"selected\"";} ?>>Nov</option>
          <option value="12" <?php if ($sess[1]['month_to'] == 12) {echo "selected=\"selected\"";} ?>>Dec</option>
        </select></td>
      </tr>
      <tr>
        <td>3 
          <input name="term[]" type="hidden" id="term[]" value="3" /></td>
        <td><select name="month_from[]">
            <option value="1" <?php if ($sess[2]['month_from'] == 1) {echo "selected=\"selected\"";} ?>>Jan</option>
            <option value="2" <?php if ($sess[2]['month_from'] == 2) {echo "selected=\"selected\"";} ?>>Feb</option>
            <option value="3" <?php if ($sess[2]['month_from'] == 3) {echo "selected=\"selected\"";} ?>>Mar</option>
            <option value="4" <?php if ($sess[2]['month_from'] == 4) {echo "selected=\"selected\"";} ?>>Apr</option>
            <option value="5" <?php if ($sess[2]['month_from'] == 5) {echo "selected=\"selected\"";} ?>>May</option>
            <option value="6" <?php if ($sess[2]['month_from'] == 6) {echo "selected=\"selected\"";} ?>>Jun</option>
            <option value="7" <?php if ($sess[2]['month_from'] == 7) {echo "selected=\"selected\"";} ?>>Jul</option>
            <option value="8" <?php if ($sess[2]['month_from'] == 8) {echo "selected=\"selected\"";} ?>>Aug</option>
            <option value="9" <?php if ($sess[2]['month_from'] == 9) {echo "selected=\"selected\"";} ?>>Sep</option>
            <option value="10" <?php if ($sess[2]['month_from'] == 10) {echo "selected=\"selected\"";} ?>>Oct</option>
            <option value="11" <?php if ($sess[2]['month_from'] == 11) {echo "selected=\"selected\"";} ?>>Nov</option>
            <option value="12" <?php if ($sess[2]['month_from'] == 12) {echo "selected=\"selected\"";} ?>>Dec</option>
          </select>
			-
        <select name="month_to[]">
          <option value="1" <?php if ($sess[2]['month_to'] == 1) {echo "selected=\"selected\"";} ?>>Jan</option>
          <option value="2" <?php if ($sess[2]['month_to'] == 2) {echo "selected=\"selected\"";} ?>>Feb</option>
          <option value="3" <?php if ($sess[2]['month_to'] == 3) {echo "selected=\"selected\"";} ?>>Mar</option>
          <option value="4" <?php if ($sess[2]['month_to'] == 4) {echo "selected=\"selected\"";} ?>>Apr</option>
          <option value="5" <?php if ($sess[2]['month_to'] == 5) {echo "selected=\"selected\"";} ?>>May</option>
          <option value="6" <?php if ($sess[2]['month_to'] == 6) {echo "selected=\"selected\"";} ?>>Jun</option>
          <option value="7" <?php if ($sess[2]['month_to'] == 7) {echo "selected=\"selected\"";} ?>>Jul</option>
          <option value="8" <?php if ($sess[2]['month_to'] == 8) {echo "selected=\"selected\"";} ?>>Aug</option>
          <option value="9" <?php if ($sess[2]['month_to'] == 9) {echo "selected=\"selected\"";} ?>>Sep</option>
          <option value="10" <?php if ($sess[2]['month_to'] == 10) {echo "selected=\"selected\"";} ?>>Oct</option>
          <option value="11" <?php if ($sess[2]['month_to'] == 11) {echo "selected=\"selected\"";} ?>>Nov</option>
          <option value="12" <?php if ($sess[2]['month_to'] == 12) {echo "selected=\"selected\"";} ?>>Dec</option>
        </select></td>
      </tr>
      <tr>
	<td>&nbsp;</td>
   	<td><input type="submit" value="Save"/></td>
      </tr>
    </table>
  </form>
</div>

            </div>
        </div>
    </div>
</body>
</html>
