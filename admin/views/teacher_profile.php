<?php include('includes/header.php'); ?>
<h1><?php echo $teacher[0]['firstname']," ",$teacher[0]['lastname']; ?></h1>
    <div>
        <div style="float:left; width:140px;">
        	<?php include("includes/teacher-action.php"); ?>
        </div>
        <div style="float:right; width:450px;">
        	<?php include($include_page); ?> 
        </div>
    </div>
</div> <!-- end of main -->

<div class="sidebar right">
	<div class="box rounded">
    	<span class="title">What would you like to do?</span>
        <span class="hint">Click on what you want to do to begin</span>
        <div><a href="teachers.php?action=add_teacher">Add a New Teacher</a></div>
        <div>Search for a Teacher</div>
        <div><input type="text" style="width:193px; height:25px;" id="search_string" onkeyup="searchTeacher();"/></div>
        <span class="hint">Enter teacher information in the box above to search for teacher. Results found will be dsiplayed in the left side of this page</span>
    </div>
</div>
<div style="clear:both"></div>

