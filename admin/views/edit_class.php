<?php include('includes/header.php'); ?>
				 <form action="classes.php?action=update_class" method="post" name="update_class">
                <table width="98%" border="0" align="center" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
                  <tr>
                    <td colspan="2" align="left"><h2>Edit Class </h2></td>
                  </tr>
                  <tr>
                    <td width="22%">Name: </td>
                    <td width="78%"><label>
                      <input name="class_id" type="text" id="class_id" value="<?php echo $class[0]['class_id']; ?>" />
                      </label>
                      <input name="id" type="hidden" id="id" value="<?php echo $class[0]['class_id']; ?>" />        </td>
                  </tr>
                  <tr>
                    <td>Teacher: </td>
                    <td><label>
                    <select name="teacher_id" id="teacher_id">
                    <option value="">..select..</option>
                    <?php
                    for($i = 0; $i < count($teacher_list); $i++)
                    {
                        if($class[0]['teacher_id'] != $teacher_list[$i]['id'])
                        echo "<option value=\"{$teacher_list[$i]['id']}\">{$teacher_list[$i]['firstname']} {$teacher_list[$i]['lastname']}</option>";
                        else
                            echo "<option value=\"{$teacher_list[$i]['id']}\" selected=\"selected\">{$teacher_list[$i]['firstname']} {$teacher_list[$i]['lastname']}</option>";
                    }
                    ?>
                    </select>
                    </label></td>
                  </tr>
                  
                  
                  <tr>
                    <td>&nbsp;</td>
                    <td><label>
                      <input type="submit" name="Submit" value="Update" />
                    </label></td>
                  </tr>
                </table>
              </form>
          </div> <!-- end of main -->

            <div class="sidebar right">
            	<div class="box rounded">
                	<span class="title">Hint &amp; Tips</span>
                    <div class="hint">To add a class, use the form on the first row of the table in the left side of the page</div>
                  	<div class="hint">To change a subject's teacher or allocate subjects to a class, click on the class name on the list in the left side of this page</div>
                    <div class="hint">You can assign teacher to teach multiple subjects to multiple classes, so take advantage of that.</div>
              </div>
          </div>
            <div style="clear:both"></div>
