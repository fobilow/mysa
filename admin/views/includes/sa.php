<table width="90%" border="0" align="center" cellpadding="5" cellspacing="0" id="student-profile-action">
  <tr>
    <th>Action</th>
  </tr>
  <tr>
    <td><a href="students.php?action=edit_student&amp;id=<?php echo $student[0]['id']; ?>">Edit   Information </a></td>
  </tr>
  <tr>
    <td>
    <?php if(student::is_registered($student[0]['id']) == "No") {?>
    <a href="students.php?action=registration_form&amp;student_id=<?php echo $student[0]['id']; ?>">Register </a>
    <?php } else {?>
    <a href="students.php?action=unregister&amp;student_id=<?php echo $student[0]['id']; ?>">Unregister </a>
    <?php } ?>
    </td>
  </tr>
  <tr>
    <td><a href="students.php?action=drop_pick_subject&amp;student_id=<?php echo $student[0]['id']; ?>&amp;class=<?php echo $student_obj->get_current_class($student[0]['id']);?>">Drop/Pick Subjects</a> </td>
  </tr>
  <tr>
    <td><a href="students.php?action=student_profile&amp;id=<?php echo $student[0]['id']; ?>">View  Profile</a></td>
  </tr>
  <tr>
    <td><a href="students.php?action=view_grades&amp;student_id=<?php echo $student[0]['id']; ?>">View Grades</a> </td>
  </tr>
  <tr>
    <td><a href="students.php?action=view_attendance&amp;student_id=<?php echo $student[0]['id']; ?>">View Attendance</a> </td>
  </tr>
  <tr>
    <td><a href="students.php?action=view_parent&amp;parent_id=<?php echo $student[0]['parent_id']; ?>&amp;student_id=<?php echo $student[0]['id']; ?>">Parent Information</a></td>
  </tr>
  <tr>
    <td><a href="students.php?action=reset_password&amp;student_id=<?php echo $student[0]['id']; ?>">Reset Password</a></td>
  </tr>
  <tr>
    <td><a href="students.php?action=confirm_delete_student&amp;student_id=<?php echo $student[0]['id']; ?>">Delete</a></td>
  </tr>
</table>