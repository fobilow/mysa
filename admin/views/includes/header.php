<?php 
$menu = array('Dashboard' => 'index.php', 
		  'Students' => 'students.php' , 
		  'Teachers' => 'teachers.php' ,
		  'Classes' => 'classes.php' , 
		  'Subjects' => 'subjects.php' , 
		  /*'Timetable' => 'timetable.php' , */
		  'Reports' => 'report.php',
		  'Notices' => 'notices-events.php' , 
		  'Events' => 'notices-events.php?#tabs-2',
		  'My Account' => 'myaccount.php'
		  );

$css = array('Dashboard' => 'dash', 
	  'Students' => 'students' , 
	  'Teachers' => 'teachers' ,
	  'Classes' => 'classes' , 
	  'Subjects' => 'subjects' , 
	  /*'Timetable' => 'timetable' , */
	  'Reports' => 'reports',
	  'Notices' => 'notices' , 
	  'Events' => 'events' , 
	  'My Account' => 'myaccount',
	  'Customize' => 'customize'
	  );

//script name
$script_name = $_SERVER['SCRIPT_NAME'];
$sn = substr($script_name,strrpos($script_name,'/')+1,strlen($script_name));

$page = array_keys($menu, $sn);

 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin | <?php echo $page[0]; ?></title>
<link href="../css/ui.css" rel="stylesheet" type="text/css" />
<!-- <link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" /> -->
<script src="../shared/jquery/js/jquery-1.3.2.min.js" type="text/javascript" language="javascript"></script>
<script src="../shared/jscripts/userExperience.js" type="text/javascript" language="javascript"></script>
<link rel="shortcut icon" href="ico.png" />
<?php include('jscript_include.php');?>
<script type="text/javascript">
$(function(){
if($('.box').length == 0)
{
  $('.main').css('width','77%');
}
})
</script>
</head>
<body>
<?php
$school_obj = new school();
$sch_info = $school_obj->select_all();

$session_obj = new session();
$csession = $session_obj->get_current_session_term();
?>

<div class="header">
	<div class="wrapper">
	<span style="padding:10px; display:block; float:left; font-weight:bold; text-shadow: 0 1px white; font-size:16px;"><?php echo $sch_info[0]['school_name']; ?></span>
	<div id="user-logout"><?php echo $_SESSION['userinfo']['name']; ?> |<a style="color:#F00; padding:0.2em;" href="logout.php">Log out</a></div>
	<div style="clear:both;"></div>
</div>
</div>
<div class="wrapper">
	<div class="sidebar">
		<span class="school-term"><?php echo to_sequence($csession['term']);?> Term, <?php echo $csession['session'];?></span>
		<div class="menu">
		<?php

		
		if(ENABLE_ACCOUNT === true)
		{
			$menu['My Account'] = 'myaccount.php';
			$menu['Customize'] = 'myaccount.php';
			$css['My Account'] = 'myaccount';
			$css['Customize'] = 'customize';
		}

		echo "<ul>";
		foreach($menu as $page => $link ){
			if($sn == $link)
				echo "<li><span class=\"$css[$page]\"></span><a href=\"$link\" id=\"current\" >$page</a></li>";
			else
				echo "<li><span class=\"$css[$page]\"></span><a href=\"$link\">$page</a></li>";	
		}
		echo "</ul>";
		?>
		</div>
		<div class="stats">
	    	<span class="title"><strong>Student Statistics</strong></span>
	   		<div>Total # of students: <?php echo student::get_total_students(); ?></div>
	        <div>Total # of boys: <?php echo student::get_total_boys(); ?></div>
	        <div>Total # of girls: <?php echo student::get_total_girls(); ?></div>
		</div>

	</div>
	<div class="main">

