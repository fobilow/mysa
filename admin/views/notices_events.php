
	<?php include("includes/header.php"); ?>

  
  <div id="tabs">
	<ul>
		<li><a href="#tabs-1">Post a Notice</a></li>
		<li><a href="#tabs-2">New Event *</a></li>
	</ul>
	<div id="tabs-1">
	<?php include("post-notice.php"); ?>
	</div>
	<div id="tabs-2">
	<?php include("add_event.php"); ?>
	</div>
</div>	
  <p>&nbsp;</p>
  <table width="95%" border="0" cellpadding="5" cellspacing="5" id="events">
      <tr>
        <td><h3>Events</h3></td>
      </tr>
	  <?php if(count($events) > 0): ?>
      <?php for($i = 0; $i < count($events); $i++): ?>
      <tr>
        <td class="event">
		<a href="notices-events.php?action=view_event&amp;event_id=<?php echo $events[$i]['event_id']; ?>"><?php echo $events[$i]['event_name']; ?> - <?php echo readable_date2($events[$i]['event_date']); ?></a>
		<a href="notices-events.php?action=delete_event&amp;event_id=<?php echo $events[$i]['event_id']; ?>" class="notice-icons">
				<img src="../images/delete.png" width="16" height="16" border="0" title="delete this post" />
		  </a>
			<a href="notices-events.php?action=edit_event&amp;event_id=<?php echo $events[$i]['event_id']; ?>" class="notice-icons">
				<img src="../images/edit.png" width="16" height="16" border="0" title="edit this post" />
			</a>
		</td>
      </tr>
      <?php endfor; ?>
	  <?php endif; ?>
  </table>
	<p>&nbsp;</p>
	<table width="95%" border="0" cellpadding="5" cellspacing="5">
      <tr>
        <td valign="bottom"><h3>Notices  </h3></td>
      </tr>
	   <?php if(count($notices) > 0): ?>
          <?php foreach($notices as $notice): ?>
		  <tr>
		  	<td valign="bottom" class="notice">
		 	<b><?php echo $notice['title']; ?> </b>
			<a href="notices-events.php?action=delete_notice&amp;id=<?php echo $notice['id']; ?>" class="notice-icons">
				<img src="../images/delete.png" width="16" height="16" border="0" title="delete this post" />
			</a>
			<a href="notices-events.php?action=edit_notice&amp;id=<?php echo $notice['id']; ?>" class="notice-icons">
				<img src="../images/edit.png" width="16" height="16" border="0" title="edit this post" />
			</a>
		 	<p><?php echo $notice['body']; ?></p>
			<p style="font-size:12px"><i>posted on <?php echo $notice['date_posted']; ?> by <?php echo $notice['posted_by']; ?></i></p>		    </td>
      	  </tr>
		<?php endforeach; ?>	
		<?php endif; ?>
  </table>
	<p>&nbsp;</p>
</div>


<script language="javascript">
$(function()
{
	populateElement('#notice-title', 'Title');
	populateElement('#notice-body', 'Type your notice here...');
});
</script>

