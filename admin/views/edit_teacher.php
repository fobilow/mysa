
 <form action="teachers.php?action=update_teacher" method="post" enctype="multipart/form-data" name="add_staff" id="add_staff">
    <table width="100%" border="0" cellpadding="5" cellspacing="0">
      <tr>
        <td valign="top">Passport Photo: </td>
        <td><label>
          <input name="picture" type="file" id="picture" size="20" />
        </label></td>
      </tr>
      <tr>
        <td valign="top">ID:</td>
        <td><label>
          <input name="teacher_id" type="text" id="teacher_id" value="<?php echo $teacher[0]['teacher_id']; ?>" size="30" />
          <input name="id" type="hidden" id="id" value="<?php echo $teacher[0]['id']; ?>" />
        </label></td>
      </tr>
      <tr>
        <td width="30%" valign="top">First name: </td>
        <td width="70%"><label>
          <input name="firstname" type="text" id="firstname" value="<?php echo $teacher[0]['firstname']; ?>" size="30" />
        </label></td>
      </tr>
      <tr>
        <td valign="top">Last name: </td>
        <td><label>
          <input name="lastname" type="text" id="lastname" value="<?php echo $teacher[0]['lastname']; ?>" size="30" />
        </label></td>
      </tr>
      
      <tr>
        <td valign="top">Email address: </td>
        <td><label>
          <input name="email" type="text" id="email" value="<?php echo $teacher[0]['email']; ?>" size="30" />
        </label></td>
      </tr>
      <tr>
        <td valign="top">Phone number: </td>
        <td><label>
          <input name="phoneno" type="text" id="phoneno" value="<?php echo $teacher[0]['phoneno']; ?>" size="30" />
        </label></td>
      </tr>
      <tr>
        <td valign="top">Address:</td>
        <td><textarea name="address" cols="30" rows="5" id="address"><?php echo $teacher[0]['address']; ?></textarea></td>
      </tr>
      
      
      <tr>
        <td valign="top">&nbsp;</td>
        <td><label>
          <input type="submit" name="Submit" value="Update" />
        </label></td>
      </tr>
    </table>
  </form>