<?php include('includes/header.php'); ?>
              <h1>New Teacher</h1>
            	<form action="teachers.php?action=add_teacher" method="post" enctype="multipart/form-data" name="add_staff" id="add_staff">
                <table width="95%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
                  <tr>
                    <td>Passport Photo: </td>
                    <td><label class="">
                      <input name="picture" type="file" id="picture" size="20" />
                    </label></td>
                  </tr>
                  <tr>
                    <td>Teacher ID:</td>
                    <td><label class="error">
                      <input name="teacher_id" type="text" id="teacher_id" size="30" />
                      <?php echo $errors['teacher_id']; ?>
                    </label></td>
                  </tr>
                  <tr>
                    <td width="22%">First name: </td>
                    <td width="78%"><label class="error">
                      <input name="firstname" type="text" id="firstname" size="30" />
                    <?php echo $errors['firstname']; ?>
                    </label></td>
                  </tr>
                  <tr>
                    <td>Last name: </td>
                    <td><label class="error">
                      <input name="lastname" type="text" id="lastname" size="30" />
                    <?php echo $errors['lastname']; ?>
                    </label></td>
                  </tr>
                  
                  <tr>
                    <td>Email address: </td>
                    <td><label class="error">
                      <input name="email" type="text" id="email" size="30" />
                      <?php echo $errors['email']; ?>
                    </label></td>
                  </tr>
                  <tr>
                    <td>Phone number: </td>
                    <td><label>
                      <input name="phoneno" type="text" id="phoneno" size="30" />
                      <?php echo $errors['phoneno']; ?>
                    </label></td>
                  </tr>
                  <tr>
                    <td>Address:</td>
                    <td><textarea name="address" cols="30" rows="5" id="address"></textarea></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td><label>
                      <input type="submit" name="Submit" value="Submit" />
                    </label></td>
                  </tr>
                </table>
              </form>
            </div> <!-- end of main -->

            <div class="sidebar right">
            	<div class="box rounded">
                	<span class="title">What would you like to do?</span>
                    <span class="hint">Click on what you want to do to begin</span>
                  <div><a href="teachers.php?action=add_teacher">Add a New Teacher</a></div>
                    <div>Search for a Teacher</div>
                    <div><input style="width:193px; height:25px;" name="search_string" type="text" id="search_string" size="40" onkeyup="searchTeacher();" /></div>
                    <span class="hint">Enter student information in the box above to search for student. Results found will be dsiplayed in the left side of this page</span>
                </div>
            </div>
            <div style="clear:both"></div>

