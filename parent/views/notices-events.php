<?php
$event_obj = new event();
$events = $event_obj->select_all(array('event_id','event_name','event_date'));

$notice_obj = new notice();
$notices = $notice_obj->select_all(array('id','title'));
?>
<div style="width:95%; margin-left:auto; margin-right:auto;">
      <table width="50%" border="0" cellpadding="5" cellspacing="0">
        <tr>
          <th>Notices</th>
        </tr>
		<?php if(count($notices) > 0): ?>
        <?php foreach($notices as $notice): ?>
	    <tr>
          <td><a href="index.php?action=view_notice&amp;id=<?php echo $notice['id']; ?>"><?php echo $notice['title']; ?></a></td>
        </tr>
		<?php endforeach; ?>
		<?php endif; ?>
    </table>
</div>
<p>&nbsp;</p>
<div style="width:95%; margin-left:auto; margin-right:auto;">
<table width="50%" border="0" cellpadding="5" cellspacing="0">
        <tr>
          <th>School Calendar </th>
        </tr>
		<?php if(count($events) > 0): ?>
        <?php foreach($events as $event): ?>
        <tr>
          <td><a href="index.php?action=view_event&amp;id=<?php echo $event['event_id']; ?>"><?php echo $event['event_name']; ?> - <?php echo readable_date2($event['event_date']); ?></a></td>
        </tr>
        <?php endforeach; ?>
		<?php endif; ?>
      </table>
</div>
