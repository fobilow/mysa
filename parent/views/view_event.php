<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Event: <?php echo $event[0]['event_name']; ?></title>
<link href="../css/ui.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />
</head>

<body>
	<div id="top">
	<?php include("includes/top.php"); ?>
	</div>
	<div id="navcontainer">
        <ul id="navlist">
         <?php include("includes/main-nav.php"); ?>
        </ul>
    </div>
<div id="mid-col">
  <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
    <th><?php echo $event[0]['event_name']; ?></th>
  </tr>
  <tr>
    <td><?php echo $event[0]['event_details']; ?></td>
  </tr>
  <tr>
    <td><?php echo readable_date2($event[0]['event_date']); ?>, <?php echo $event[0]['event_time']; ?></td>
  </tr>
</table>
</div>
	<div id="footer">
	myschoolassist 2009 myschoolassist.com
	</div>
</body>
</html>
