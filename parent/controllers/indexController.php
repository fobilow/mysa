<?php
class indexController extends Controller
{
	function index()
	{
		include(VIEWPATH."home.php");
	}
	
	function view_child()
	{
		//display child's mini profile
		$id = $_GET['id'];
		$student_obj = new student();
		$std = $student_obj->select($id);
		$data = $student_obj->get_grades($id,null,null);
		$subjects = $student_obj->get_subject_offered($id);
		$asst = new assesment();
		$header = $asst->assement_header();
		include(VIEWPATH."view_child.php");
	}
	
	function myaccount()
	{
		include(VIEWPATH."change_password.php");
	}

    function change_password()
	{
		if(!isset($_POST['Submit']))
		{
			include(VIEWPATH."change_password.php");
		}
		else
		{
			$user_obj = new user();
			$user_obj->change_password("parents");
		}
	}

        function notices()
        {
            $notice_obj = new notice();
            $notices = $notice_obj->select_all(array('id','title'));
            include(VIEWPATH."notices.php");
        }

        function events()
        {
            $event_obj = new event();
            $events = $event_obj->select_all(array('event_id','event_name','event_date'));
            include(VIEWPATH."events.php");
        }

        function view_notice()
        {
            $id = $_GET['id'];
            $notice_obj = new notice();
            $notice = $notice_obj->select($id);
            include(VIEWPATH."view_notice.php");
        }

        function view_event()
        {
            $id = $_GET['id'];
            $event_obj = new event();
            $event = $event_obj->select($id);
            include(VIEWPATH."view_event.php");
        }
}

?>