<?php
require_once("bootstrap.php");
require_once("../config.php");
require_once("../shared/lib/common.php");

$school_name = host::start_app();

define('SCHOOL_NAME',$school_name);

if(!isset($_SESSION))
session_start();

if(!$_SESSION['parent_login'])
	header("Location: login.php");


$index_front = new indexController();
$index_front->run(); 

?>
