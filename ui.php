<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin | Home</title>
<link href="ui.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="website/images/ico.png" />
</head>

<body>
	<div id="top">
	<?php include("topp.php"); ?>
	</div>	
	<div id="navcontainer">
        <ul id="navlist">
          <li><a href="#">Home</a></li>
          <li><a href="#">Students</a></li>
          <li><a href="#">Teachers</a></li>
          <li class="style1"><a href="#">Subjects</a></li>
          <li><a href="#">Classes</a></li>
          <li><a href="#">Report</a></li>
		  <li><a href="#">My Account</a></li>
		  <li><a href="#">My Settings</a></li>
		  <li><a href="#">Help</a></li>
        </ul>
    </div>
<div id="mid-col">
<?php include("p.php"); ?>
</div>
	<div id="footer">
	  <p>&reg;myschoolassist 2010</p>
	  <p> <a href="http://myschoolassist.com">myschoolassist.com	</a></p>
	  <p>A product of B.E Systems</p>
	  <p><a href="#">Home</a> | <a href="#">Students</a> | <a href="#">Teachers</a> | <a href="#">Subjects</a> | <a href="#">Classes</a> | <a href="#">Report</a> | <a href="#">My Account</a> | <a href="#">My Settings</a> | <a href="#">Help</a> </p>
	  <p>Read the <a href="#">Terms &amp; Conditions</a> </p>
	</div>
</body>
</html>
