<?php

//MySchoolAssist environment 
//possible values - dev, live
define("ENV","dev"); 

//Sudomain validation
//possible valueson - on, off. 
//Set to off if you do not want to use subdomain to vailidate users
define("SUBDOMAIN","off"); //

//Name of the school. Only used if subdomain is off
define("SCHOOLNAME", "School Name"); 

//username of school. Only used if subdomain is off
define("SCHOOL", "schoolName"); 

//menu to hide from user
$hide_menu = array('myaccount.php','notices-events.php','help.php');

?>
