<?php
require_once("bootstrap.php");
require_once("../config.php");

$school = host::load_db();

if(!isset($_SESSION))
	session_start();

if($_SESSION['teacher_login'])
	header("Location: index.php");
	
if(isset($_POST['Submit']) && $_POST['Submit'] == "Login")
{
	
	$uname = $_POST['username'];
	$pswd = trim($_POST['password']);
    
    $login = new login($uname,$pswd);
    $login->set_table("teachers");
    $login->set_columns("teacher_id","password");
    if($login->authenticate())
    {	
	    	$_SESSION['teacher_login'] = true; 
	        $_SESSION['userinfo'] = $login->session_data;
			
			$school_obj = new school();
			$theme = $school_obj->select_all(array('theme'));
			$_SESSION['theme'] = $theme[0]['theme'];
		
			header("location: index.php");
	}
	else
	{
		$msg = $login->get_error();
		$_SESSION['login_err'] = 1;
		header("location: ../index.php?user=teacher");
	}
}

ob_start();
?>
