<?php
class myaccountController extends Controller
{
	function index()
	{
		$id = $_SESSION['userinfo']['id'];
		$teacher_obj = new teacher();
		
		$teacher = $teacher_obj->select($id);
		$subjects = $teacher_obj->get_subjects_taught($id);
		include(VIEWPATH."myaccount.php");
	}
	
	
	function change_password()
	{
		if(!isset($_POST['Submit']))
		{
                    include(VIEWPATH."change_password.php");
		}
		else
		{
                    $user_obj = new user();
                    $user_obj->change_password("teachers");
		}
	}
}
?>