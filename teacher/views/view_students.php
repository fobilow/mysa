<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Students</title>
<link href="../css/thirdeye.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />
<script src="../../shared/jscripts/userExperience.js" type="text/javascript" language="javascript"></script>
</head>

<body>
	<div id="top">
	<?php include("includes/top.php"); ?>
	</div>
	<?php include("includes/main-nav.php"); ?>
	<div id="left-col">
	<?php include("includes/left-side.php"); ?>
	</div>
	<div id="mid-col">
<form name="studentlist" method="post" action="">
<table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
	<td colspan="5"><h2><strong>Students</strong></h2>
	  </td>
  </tr>
  <tr>
	<td colspan="5">
	  <input type="submit" name="editAction" value="Edit..." id="edit" disabled="disabled"
	  onclick="document.studentlist.action='students.php?action=edit_student';" />
	  <input type="submit" name="deleteAction" value="Delete" id="delete" disabled="disabled"
	  onclick="document.studentlist.action='students.php?action=delete_student';" />
	  </td>
  </tr>
  <tr>
	<th width="3%"><label>
	  <input type="checkbox" name="checkbox" value="checkbox" onclick="checkAll('student_id[]',this)" />
	</label></th>
	<th width="43%">Name</th>
	<th width="29%">&nbsp;</th>
	<th width="12%">Registered</th>
	<th width="13%">Status</th>
  </tr>
  <?php
  for($i = 0; $i < count($students); $i++)
  { ?>
  <tr bgcolor="<?php echo color($i); ?>">
	<td><label>
	  <input type="checkbox" name="student_id[]" value="<?php echo $students[$i]['id']; ?>" 
	  onclick="toggle_edit_delete('student_id[]','edit','delete')" />
	</label></td>
	<td><a href="students.php?action=student_profile&id=<?php echo $students[$i]['id']; ?>"><?php echo $students[$i]['firstname'],' ',$students[$i]['lastname']; ?></a></td>
	<td width="29%"><a href="students.php?action=reset_password&amp;student_id=<?php echo $students[$i]['id']; ?>">reset password</a></td>
	<td width="12%"><?php echo student::is_registered($students[$i]['id']); ?></td>
	<td bgcolor="<?php echo color($i); ?>"><strong><img src="../images/<?php echo $students[$i]['status']; ?>.gif" width="65" height="15"/></strong></td>
  </tr>
  <?php } ?>
</table>
  </form>
	</div>
	<div id="right-col">
	<?php include("includes/right-side.php"); ?>
	</div>
	<div id="footer">
	myschoolassist 2009 myschoolassist.com
	</div>
</body>
</html>
