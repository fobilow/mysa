<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Confirm Delete</title>
<link href="../css/thirdeye.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="top">
  <?php include("includes/top.php"); ?>
</div>
 <?php include("includes/main-nav.php"); ?>
<div id="left-col">
  <table width="150" border="0" id="student-photo">
  <tr>
    <td>
	<img src="<?php echo $student[0]['picture']; ?>" width="150" height="156"/>
	</td>
  </tr>
</table>
<br />
  <table width="90%" border="0" align="center" id="session">
  <tr>
    <td valign="top"><p><strong>Session: </strong>2009/2010<?php echo $session[0]['session'];?></p>
      <p><strong>Term: </strong>1st <?php echo $session[0]['term'];?></p>
	</td>
  </tr>
</table>
<br />
<?php include("includes/student-action.php"); ?>
</div>
<div id="mid-col">
  <table width="95%" border="0" align="center" cellpadding="3" cellspacing="0">
    <tr>
      <td colspan="2" valign="top"><h2>Confirm Delete </h2>
      <p>You are about to delete this student. If you delete this student all information about this student will be lost. If you wish to continue with this action. Click on the OK button 
        <label>
        <br />
        <br />
        </label>
      </p>
      <form id="form1" name="form1" method="post" action="students.php?action=delete_student&amp;student_id=<?php echo $student[0]['id']; ?>">
        <label>
        <input type="submit" name="Submit" value="Ok" />
          </label>
        <input type="button" name="Button" value="Cancel" onclick="document.location.href = 'students.php?action=student_profile&amp;id=<?php echo $student[0]['id']; ?>'" />
        <input name="student_id" type="hidden" id="student_id" />
      </form>      </td>
    </tr>
  </table>    
</div>
<!--<div id="right-col">
  <?php include("right-side.php"); ?>
</div>
--><div id="footer"> myschoolassist 2009 myschoolassist.com </div>
</body>
</html>
