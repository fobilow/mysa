<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Students</title>
<link href="../css/thirdeye.css" rel="stylesheet" type="text/css" />
<?php include('includes/jscript_include.php');?>
</head>

<body>
	<div id="top">
	<?php include("includes/top.php"); ?>
	</div>
	<?php include("includes/main-nav.php"); ?>
	<div id="main-col"><br />
<form name="studentlist" method="post" action="">
<table width="70%" border="0" align="center" cellpadding="5" cellspacing="0">
  <tr>
	<td colspan="5"><strong>Search Results for &quot;<?php echo $searched; ?>&quot;</strong></td>
  </tr>
  <tr bgcolor="#CC3300">
	<th width="72%">Name</th>
	<th width="12%">Registered</th>
	<th width="13%">Status</th>
  </tr>
  <?php
  for($i = 0; $i < count($students); $i++)
  { ?>
  <tr bgcolor="<?php echo color($i); ?>">
	<td><a href="index.php?action=student_profile&id=<?php echo $students[$i]['id']; ?>"><?php echo $students[$i]['firstname'],' ',$students[$i]['lastname']; ?></a></td>
	<td width="12%"><?php echo student::is_registered($students[$i]['id']); ?></td>
	<td bgcolor="<?php echo color($i); ?>"><strong><img src="../images/<?php echo $students[$i]['status']; ?>.gif" width="65" height="15"/></strong></td>
  </tr>
  <?php } ?>
</table>
  </form>
</div>
	<div id="footer">
	myschoolassist 2009 myschoolassist.com
	</div>
</body>
</html>
