<?php include('includes/header.php'); ?>
 <h1>Student Reports</h1>   
   <form action="" method="post">
 	   <table width="98%" border="0" align="center" cellpadding="5" bgcolor="#FFFFFF";> 
      	<td>Select Session: </td>
          <td>
          <select  name="session" id="report-session" style="width:100px;">
            <option value="" selected="selected">-select-</option>
            <option value="2009/2010" >2009/2010</option>
            <option value="2010/2011" >2010/2011</option>
            <option value="2011/2012" >2011/2012</option>
            <option value="2012/2013" >2012/2013</option>
            <option value="2013/2014" >2013/2014</option>
            <option value="2014/2015" >2014/2015</option>
            <option value="2015/2016" >2015/2016</option>
            <option value="2016/2017" >2016/2017</option>
            <option value="2017/2018" >2017/2018</option>
            <option value="2018/2019" >2018/2019</option>
            <option value="2019/2020" >2019/2020</option>
          </select>
          </td>
      </tr>
      <tr>
        <td>Select Term: </td>
        <td width="82%">
        <select name="term" id="report-term" style="width:100px;">
          <option>-select-</option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
              </select></td>
        </tr>
      <tr>
        <td>Select Class: </td>
        <td>
        <select name="class_id" id="report-class" style="width:100px;">
          <option>-select-</option>
          <?php
					for($i = 0; $i < count($classes); $i++)
					{
						echo "<option value=\"{$classes[$i]['class_id']}\">{$classes[$i]['class_id']}</option>";
					}
					?>
          </select></td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input name="submit" type="button"  onclick="generateReport()" value="Generate Report" /></td>
        </tr>
      </table>
    </form>
</div> <!-- end of main -->
