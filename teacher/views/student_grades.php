<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo $student[0]['firstname']," ",$student[0]['lastname']; ?> Grades</title>
<link href="../css/thirdeye.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="top">
  <?php include("includes/top.php"); ?>
</div>
<?php include("includes/main-nav.php"); ?>
<div id="left-main-col">
  <table width="150" border="0" id="student-photo">
  <tr>
    <td>
	<img src="<?php echo $student[0]['picture']; ?>" width="150" height="156"/>
	</td>
  </tr>
</table>
<br />
  <table width="90%" border="0" align="center" id="session">
  <tr>
    <td valign="top"><p><strong>Session: </strong>2009/2010<?php echo $session[0]['session'];?></p>
      <p><strong>Term: </strong>1st <?php echo $session[0]['term'];?></p>
	</td>
  </tr>
</table>
<br />
<?php include("includes/student-action.php"); ?>
</div>
<div id="right-main-col">
  <table width="95%" border="0" align="center" cellpadding="3" cellspacing="0">
    <tr>
      <td colspan="2" valign="top"><h2>Grades: <?php echo $student[0]['firstname']," ",$student[0]['lastname']; ?></h2></td>
    </tr>
  </table>    
  <table width="95%" border="0" align="center" cellpadding="5" cellspacing="2">
    <tr>
      <th width="46%">Subject Name</th>
      <?php echo $asst->assement_header(); ?>
      <th width="14%">Total</th>
    </tr>
	<?php for($i= 0; $i < count($data); $i++) {?>
    <tr bgcolor="<?php echo color($i); ?>">
      <td><?php echo $data[$i]['name']; ?></td>
     <?php echo $asst->assesment_scores($data[$i]); ?>
      <td><?php echo $data[$i]['total']; ?></td>
    </tr>
	<?php }?>
  </table>
</div>
<div id="footer"> myschoolassist 2009 myschoolassist.com </div>
</body>
</html>
