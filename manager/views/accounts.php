<div id="submenu">
<a href="index.php?t=1&tab=account&action=create">Create account </a>
<a href="index.php?t=1&tab=account" id="curr">View account </a>
</div>
<div id="white">
<div id="body">
<br />
<form name="accountlist" method="post" action="">
<h2>Accounts</h2>
<p align="right"><a href="download_db_setup.php">Download the lastest the database setup</a></p>
<table width="100%" border="0" cellpadding="5" cellspacing="0">
<tr>
  <td colspan="7">
   <input type="submit" name="action1" value="Edit..." id="edit" disabled="disabled" 
   onclick="document.accountlist.action='index.php?t=1&tab=account&action=edit';">
   <input type="submit" name="action2" value="Delete" id="delete" disabled="disabled"
   onclick="document.accountlist.action='index.php?t=1&tab=account&action=delete';">  </td>
  </tr>
<tr bgcolor="#FFCC33">
  <td width="3%"><strong>
    <label>
    <input type="checkbox" name="checkbox" value="checkbox" onclick="checkAll('account[]',this)" />
      </label>
  </strong></td>
<td width="28%"><strong>School name</strong></td>
<td width="11%"><strong>No of students</strong></td>
<td width="25%"><strong>Email</strong></td>
<td width="12%"><strong>URL</strong></td>
<td width="7%"><strong>Status</strong></td>
<td width="14%">&nbsp;</td>
</tr>
<?php for($i = 0; $i < count($accounts); $i++) 
{
	$status = $accounts[$i]['status'];
 ?>
<tr bgcolor="<?php echo color($i); ?>">
  <td><label>
    <input type="checkbox" name="account[]" value="<?php echo $accounts[$i]['url']?>"
	onclick="toggle_edit_delete('account[]','edit','delete')" />
  </label></td>
<td><?php echo $accounts[$i]['school_name']; ?></td>
<td><?php echo $accounts[$i]['school_students']; ?></td>
<td><?php echo $accounts[$i]['email']?></td>
<td><?php echo $accounts[$i]['url']?></td>
<td><?php echo $accounts[$i]['status']?></td>
<td>
<?php if($status == 'active') { ?>
<a href="index.php?t=1&tab=account&action=deactivate&id=<?php echo $accounts[$i]['url']; ?>" title="deactivate this account">deactivate</a>
<?php } else { ?>
<a href="index.php?t=1&tab=account&action=activate&id=<?php echo $accounts[$i]['url']; ?>" title="activate this account">activate</a>
<?php } ?></td>
</tr>
<?php } ?>
</table>
</form>
</div>
</div>