-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 04, 2009 at 05:52 PM
-- Server version: 5.1.36
-- PHP Version: 5.2.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thirdeye-setup`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `username` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `name` varchar(100) NOT NULL,
  `role` varchar(20) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`username`, `password`, `name`, `role`) VALUES
('admin', '5f4dcc3b5aa765d61d8327deb882cf99', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `assesments`
--

CREATE TABLE IF NOT EXISTS `assesments` (
  `assesment_id` int(11) NOT NULL AUTO_INCREMENT,
  `assesment_name` varchar(100) NOT NULL,
  `real_name` varchar(100) NOT NULL,
  `max_score` int(11) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`assesment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `assesments`
--


-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE IF NOT EXISTS `attendance` (
  `session_id` varchar(32) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `entered_by` varchar(100) NOT NULL,
  PRIMARY KEY (`session_id`,`subject_id`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance`
--


-- --------------------------------------------------------

--
-- Table structure for table `calendar`
--

CREATE TABLE IF NOT EXISTS `calendar` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_name` varchar(100) NOT NULL,
  `event_date` date NOT NULL,
  `event_time` varchar(7) NOT NULL,
  `event_details` text NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `calendar`
--


-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE IF NOT EXISTS `classes` (
  `class_id` varchar(20) NOT NULL,
  `teacher_id` varchar(10) NOT NULL COMMENT 'this is the class teacher',
  `timetable` text NOT NULL,
  `description` varchar(250) NOT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes`
--


-- --------------------------------------------------------

--
-- Table structure for table `class_subjects`
--

CREATE TABLE IF NOT EXISTS `class_subjects` (
  `class_id` varchar(20) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `teacher_id` varchar(10) NOT NULL,
  PRIMARY KEY (`class_id`,`subject_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_subjects`
--


-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE IF NOT EXISTS `grades` (
  `session_id` varchar(32) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `date_entered` datetime NOT NULL,
  `entered_by` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`session_id`,`subject_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grades`
--


-- --------------------------------------------------------

--
-- Table structure for table `gradingscheme`
--

CREATE TABLE IF NOT EXISTS `gradingscheme` (
  `id` int(11) NOT NULL,
  `grade_letter` varchar(1) NOT NULL,
  `grade_min` int(2) NOT NULL,
  `grade_max` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gradingscheme`
--

INSERT INTO `gradingscheme` (`id`, `grade_letter`, `grade_min`, `grade_max`) VALUES
(1, 'A', 70, 100),
(2, 'B', 60, 69),
(3, 'C', 55, 59),
(4, 'P', 50, 54),
(5, 'F', 0, 49);

-- --------------------------------------------------------

--
-- Table structure for table `message_recieved`
--

CREATE TABLE IF NOT EXISTS `message_recieved` (
  `message id` int(11) NOT NULL AUTO_INCREMENT,
  `to` varchar(200) NOT NULL,
  `from` varchar(200) NOT NULL,
  `title` varchar(300) NOT NULL,
  `body` text NOT NULL,
  `date_time` datetime NOT NULL,
  PRIMARY KEY (`message id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `message_recieved`
--


-- --------------------------------------------------------

--
-- Table structure for table `message_sent`
--

CREATE TABLE IF NOT EXISTS `message_sent` (
  `message id` int(11) NOT NULL AUTO_INCREMENT,
  `to` varchar(200) NOT NULL,
  `from` varchar(200) NOT NULL,
  `title` varchar(300) NOT NULL,
  `body` text NOT NULL,
  `date_time` datetime NOT NULL,
  PRIMARY KEY (`message id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `message_sent`
--


-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

CREATE TABLE IF NOT EXISTS `notices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) NOT NULL,
  `body` text NOT NULL,
  `date_posted` date NOT NULL,
  `posted_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `notices`
--


-- --------------------------------------------------------

--
-- Table structure for table `parents`
--

CREATE TABLE IF NOT EXISTS `parents` (
  `parent_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile_phone` varchar(15) NOT NULL,
  `land_phone` varchar(15) NOT NULL,
  `address` text NOT NULL,
  `portal` varchar(1) NOT NULL DEFAULT 'N',
  `password` varchar(32) NOT NULL,
  PRIMARY KEY (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `parents`
--


-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE IF NOT EXISTS `school` (
  `school_name` varchar(200) NOT NULL,
  `school_admin` varchar(100) NOT NULL,
  `school_email` varchar(100) NOT NULL,
  `school_phoneno` varchar(15) NOT NULL,
  `school_address` text NOT NULL,
  `school_logo` varchar(200) NOT NULL,
  PRIMARY KEY (`school_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school`
--


-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `id` int(1) NOT NULL,
  `session` varchar(10) NOT NULL,
  `term` int(1) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session`
--


-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(40) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(1) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `phoneno` varchar(15) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `status` varchar(20) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `password` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `students`
--


-- --------------------------------------------------------

--
-- Table structure for table `student_class`
--

CREATE TABLE IF NOT EXISTS `student_class` (
  `session_id` varchar(32) NOT NULL,
  `student_id` int(11) NOT NULL,
  `class_id` varchar(20) NOT NULL,
  `session` varchar(10) NOT NULL,
  `term` int(11) NOT NULL,
  `mode` varchar(10) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_class`
--


-- --------------------------------------------------------

--
-- Table structure for table `student_subjects`
--

CREATE TABLE IF NOT EXISTS `student_subjects` (
  `session_id` varchar(32) NOT NULL,
  `subject_id` int(11) NOT NULL,
  PRIMARY KEY (`session_id`,`subject_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_subjects`
--


-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
  `subject_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `subject_outline` varchar(300) NOT NULL,
  PRIMARY KEY (`subject_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--


-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE IF NOT EXISTS `teachers` (
  `teacher_id` varchar(40) NOT NULL,
  `firstname` varchar(100) NOT NULL DEFAULT '-',
  `lastname` varchar(100) NOT NULL DEFAULT '-',
  `email` text NOT NULL,
  `phoneno` varchar(15) NOT NULL,
  `address` text NOT NULL,
  `picture` varchar(200) NOT NULL,
  `password` varchar(32) NOT NULL,
  PRIMARY KEY (`teacher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers`
--

