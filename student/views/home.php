<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Home | Student Portal</title>
<link href="../css/ui.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />-->
<script src="../shared/jquery/js/jquery-1.3.2.min.js" type="text/javascript" language="javascript"></script>
<script src="../shared/jscripts/userExperience.js" type="text/javascript" language="javascript"></script>
<script src="../shared/jscripts/mysa.js" type="text/javascript" language="javascript"></script>
<link rel="shortcut icon" href="ico.png" />
</head>
<body>
	<div id="wrapper">
    	<?php include('includes/top.php'); ?>
        <div id="menu">
        	<?php include('includes/nav.php'); ?>
        </div>
        <div id="content">
        	
        	<div id="content-main">
            	<table width="95%" border="0" align="center" cellspacing="0">
                    <tr>
                      <td colspan="2" valign="top"><h1>Welcome to Student's Portal </h1></td>
                    </tr>
                    <tr>
                      <td valign="top"><table width="150" border="0" id="student-photo">
                          <tr>
                            <td><img src="<?php echo $student[0]['picture']; ?>" width="150" height="156"/> </td>
                          </tr>
                        </table>
                          <br /></td>
                      <td valign="top"><table width="95%" border="0" align="center" cellpadding="3" cellspacing="3" bgcolor="#F1F1F1">
                          <tr>
                            <td valign="top">Status</td>
                            <td valign="top"><?php echo $student[0]['status']; ?></td>
                          </tr>
                          <tr>
                            <td valign="top">Student Name: </td>
                            <td valign="top"><?php echo $student[0]['firstname']," ",$student[0]['lastname']; ?></td>
                          </tr>
                          <tr>
                            <td width="234" valign="top">Student ID: </td>
                            <td width="493" valign="top"><strong><?php echo $student[0]['student_id']; ?></strong></td>
                          </tr>
                          <tr>
                            <td valign="top">Gender</td>
                            <td valign="top"><?php echo $student[0]['gender']; ?></td>
                          </tr>
                          <tr>
                            <td valign="top">Class: </td>
                            <td valign="top"><?php 
                    if(student::is_registered($student[0]['id']) == "Yes")
                        echo $student_obj->get_current_class($student[0]['id']);
                    elseif(student::is_registered($student[0]['id']) == "No") 
                        echo "--";
                    ?></td>
                          </tr>
                          <tr>
                            <td valign="top">Date of birth </td>
                            <td valign="top"><?php echo readable_date2($student[0]['dob']); ?></td>
                          </tr>
                        </table>
                          <br />
                          <table width="95%" border="0" align="center" cellpadding="3" cellspacing="3" bgcolor="#F1F1F1">
                            <tr>
                              <td colspan="2" valign="top"><strong>Contact Information </strong></td>
                            </tr>
                            <tr>
                              <td width="234" valign="top">Email:</td>
                              <td width="493" valign="top"><?php echo $student[0]['email']; ?></td>
                            </tr>
                            <tr>
                              <td valign="top">Phone number:</td>
                              <td valign="top"><?php echo $student[0]['phoneno']; ?></td>
                            </tr>
                            <tr>
                              <td valign="top"> Addresss:</td>
                              <td valign="top"><?php echo $student[0]['address']; ?></td>
                            </tr>
                        </table></td>
                    </tr>
                  </table>
            </div>
        </div>
    </div>
</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Student | Home</title>
<link href="../css/ui.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="ico.png" />
</head>
