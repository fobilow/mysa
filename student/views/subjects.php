<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>My Subjects</title>
<link href="../css/thirdeye.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_SESSION['theme']; ?>" rel="stylesheet" type="text/css" />
</head>

<body>
	<div id="top">
	<?php include("includes/top.php"); ?>
	</div>
	<?php include("includes/main-nav.php"); ?>
<div id="main-col">
  <table width="70%" border="0" align="center" cellpadding="3" cellspacing="0">
      <tr>
        <td width="26%" colspan="2" valign="top"><h2><strong>My Subjects</strong></h2></td>
      </tr>
	   <tr bgcolor="<?php echo color($j); ?>">
	    <th valign="top">Subject </th>
	    <th valign="top">Subject Outline</th>
    </tr>
  	  <?php if(count($subjects) > 0) {?>
	  <?php for($j= 0; $j < count($subjects); $j++) {?>
	  <tr bgcolor="<?php echo color($j); ?>">
        <td width="26%" valign="top"><?php echo $subjects[$j]['name']; ?></td>
        <?php if($subjects[$j]['subject_outline'] != ""): ?>
        <td width="74%" valign="top"><a href="<?php echo $subjects[$j]['subject_outline']; ?>">download</a></td>
	  <?php else: ?>
        <td width="74%" valign="top">Not Available</td>
        <?php endif; ?>
          </tr>
      <?php } }?>
    </table>
  <p>&nbsp;</p>
</div>
<div id="footer">
myschoolassist 2009 myschoolassist.com
</div>
</body>
</html>
